from doctest import ELLIPSIS_MARKER
import distances as dt
import pprint
pp = pprint.PrettyPrinter(indent=4)
# OPTICS ALGORITH
from sklearn.cluster import OPTICS, cluster_optics_dbscan
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import math
import argparse

import copy

PLOT_GRAPHICS = True
no_random_devs = False

# TODO : chech if random device with is matched to any global device with random bit in MAC not set
    # '''
    # @brief Transform global MAC to random MAC
    # @param oui OUI of global MAC address in string (example: "a4:50:46")
    # @return Random MAC from global MAC with local bit set
    # '''
    # def transf_rand_MAC_to_global(self, oui):
    #     # set local bit to 1
    #     oui=list(oui)
    #     oui[1]=int(oui[1], 16) | 0b10
    #     return str(oui)


RSSI_KEY = 'RSSI'
TIME_KEY = 'TIME'
SSID_KEY = 'SSID'
DATA_RTS_KEY = "DATA_RTS"
SUPP_DATA_RTS_KEY = "SUPP"
EXT_SUPP_DATA_RTS_KEY = "EXT"
HT_CAP_KEY = 'HT_CAP'
EXT_CAP_KEY = 'EXT_CAP'
VHT_CAP_KEY = 'VHT_CAP'
INTERWORKING_KEY = 'INTWORK'
VENDOR_SPEC_KEY = 'VENDOR_SPEC'
EXT_TAG_KEY = 'EXT_TAG'
DEVICE_LABEL_KEY = "LABEL"

# RSSI_K = 1.7
RSSI_K = 1.5
TIMING_KNEE = 0.1
TIMING_SCALE = 5
TIMING_OFFSET = -5

# SUPP_DATA_RATE_SCALE = 100
# EXT_SUPP_DATA_RATE_SCALE = 100
# DATA_RATE_DATA = {SUPP_DATA_RTS_KEY: 25, EXT_SUPP_DATA_RTS_KEY: 30}
DATA_RATE_DATA_SCALE = 25
NO_IE_SCALE = 15

# HT_CAP_SCALE = 642
# HT_CAP_BITS_LENGTH = 26
# EXT_CAP = 320
# VHT_CAP_SCALE = 960
# VHT_CAP_BITS_LENGTH = 96
# INTERWORKING_SCALE = 840
BITS_DATA = {	
	HT_CAP_KEY: [400,26], # [scale, num_of_bits, [no tag]]
	VHT_CAP_KEY: [600,96],
	VENDOR_SPEC_KEY: [100, lambda x:(len(x)-2)*4, 6, 8], # -2 to remove "0x" and *4 since each char represets 4 digits
	EXT_TAG_KEY: [100,  lambda x:(len(x)-2)*4, 6, 8] # -2 to remove "0x" and *4 since each char represets 4 digits
}
VAR_BITS_DATA = {
	EXT_CAP_KEY: [100, 35], # [scale, different lengths penalty] # highly variable value
	INTERWORKING_KEY: [500, 50],
}

INCLUDE_SPECIAL_IE_SCALE = -2.5
SSID_SCALE_SAME = -25
STARTING_DST = 100
MIN_DST = 88 # STARTING_DST/MIN_DST+some_margin<XI_UP #increased from 60

# OPTICS DEFINES
MIN_SAMPLES_ST_1 = 2 # number of samples in a neighborhood for a point to be considered as a core point
XI_ST_1 = 0.4 # Determines the minimum steepness on the reachability plot that constitutes a cluster boundary. For example, an upwards point in the reachability plot is defined by the ratio from one point to its successor being at most 1-xi.
MIN_CLUSTER_SIZE_ST_1 = 2
MAX_EPS_ST_1 = 200 #220 #np.inf #100 # maximum distance between two samples for one to be considered as in the neighborhood of the other
CLUSTER_METHOD_ST_1 ='xi' # extraction method used to extract clusters using the calculated reachability and ordering. Possible values are “xi” and “dbscan”
EPS_ST_1 = 20 # eps for dbscan clustering if cluster_method='dbcsan'?

XI_UP = 1.25
XI_DOWN = 0.85


SECOND_STAGE_OPTICS = False
MIN_SAMPLES_ST_2 = 2 # number of samples in a neighborhood for a point to be considered as a core point
XI_ST_2 = 0.96 # Determines the minimum steepness on the reachability plot that constitutes a cluster boundary. For example, an upwards point in the reachability plot is defined by the ratio from one point to its successor being at most 1-xi.
MIN_CLUSTER_SIZE_ST_2 = 2
MAX_EPS_ST_2 = np.inf #300 # maximum distance between two samples for one to be considered as in the neighborhood of the other
CLUSTER_METHOD_ST_2 ='xi' # extraction method used to extract clusters using the calculated reachability and ordering. Possible values are “xi” and “dbscan”
EPS_ST_2 = 20 # eps for dbscan clustering if cluster_method='dbcsan'?

DISTANCE_THRESHOLD = MAX_EPS_ST_1 # beforeSTARTING_DST + 75
KOEFF_DIFF = XI_UP  # 140/100


total_devices = 0

# TODO: ADD INFORMATION ELEMENT ORDER


parser = argparse.ArgumentParser(allow_abbrev=False)
parser.add_argument('--device', type=str, default="0000000063a2d9d9", help="Serial number of device that data will be taken into account")
args = parser.parse_args()

# "device": "S11\n",
# "serial": "00000000ad932322"
#---------------------------------
# "device": "N403\n",
# "serial": "000000007a69c602"
#---------------------------------
# "device": "Sejina\n",
# "serial": "0000000063a2d9d9"



'''
@brief Calculate total "distance" between two probe requests
@param pr_1 Probe request 1
@param pr_2 Probe request 2
@return total "distance"
'''
def dist_btwn_two_probe_req(data_1 : dict, data_2 : dict,):  # dict of type {"SSID": mac_1["SSID"],"pr": pr_1}
	# # find intersecting IE elements and calculate "dist" for each of those IE
	# # dictionary comprehension

	dst = STARTING_DST
	sep_dsts = {}

	intersecting_keys = dt.dicts_keys_intersection(
													data_1["pr"]["DATA"],
													data_2["pr"]["DATA"]
												  )
	
    
	for int_key in intersecting_keys:
        
		if int_key==DATA_RTS_KEY:
			# merge supp and ext data rates
			pr1_data_rts = []
			for data_rts_type in data_1["pr"]["DATA"][int_key]:
				pr1_data_rts.extend(data_1["pr"]["DATA"][int_key][data_rts_type])
			pr2_data_rts = []
			for data_rts_type in data_2["pr"]["DATA"][int_key]:
				pr2_data_rts.extend(data_2["pr"]["DATA"][int_key][data_rts_type])
			# find data rates that are not common between two MACs
			dst_tmp = len(dt.lists_symmetric_difference(
													 	pr1_data_rts,
													 	pr2_data_rts)\
					    )* DATA_RATE_DATA_SCALE
			dst += dst_tmp		  
			# print(f'SUPPDATA/EXTSUPPDATA : {dst_tmp}')
			sep_dsts[int_key] = dst_tmp

        
		elif int_key in [HT_CAP_KEY, VHT_CAP_KEY]:
			dst_tmp = dt.bit_dist(
								int(data_1["pr"]["DATA"][int_key],16), 
								int(data_2["pr"]["DATA"][int_key],16), 
								BITS_DATA[int_key][0], 
								BITS_DATA[int_key][1]
							   )
			dst += dst_tmp
			# print(f'HT_CAP/VHTCAP : {dst_tmp}')
			sep_dsts[int_key] = dst_tmp


		elif int_key in [EXT_CAP_KEY, INTERWORKING_KEY]:
			# calculate length of data from 0x string
			length_1 = len(data_1["pr"]["DATA"][int_key])
			length_2 = len(data_2["pr"]["DATA"][int_key])

			# if lenghts are different than we can not compare them as declare that they do not match
			if length_1 != length_2:
				dst += VAR_BITS_DATA[int_key][1]
				# print(int_key) ########################################### DEBUG PRINT
				# print(VAR_BITS_DATA[int_key][1]) ########################################### DEBUG PRINT

			else:
				dst_tmp = dt.bit_dist(
									int(data_1["pr"]["DATA"][int_key],16), 
									int(data_2["pr"]["DATA"][int_key],16), 
									VAR_BITS_DATA[int_key][0], 
									(length_1-2)*4 # -2 to remove "0x" and *4 since each char represets 4 digits
								  )
				dst += dst_tmp
				# print(f'EXT_CAP/INTER : {dst_tmp}')
				sep_dsts[int_key] = dst_tmp


		elif int_key==VENDOR_SPEC_KEY:
			# print(int_key)  ########################################### DEBUG PRINT
			dst += INCLUDE_SPECIAL_IE_SCALE
			# print("INCLUDE_SPECIAL_IE_SCALE") ########################################### DEBUG PRINT
			
			# find intersecting keys for vendor (same vendor)
			same_vendors = dt.dicts_keys_intersection(
													  data_1["pr"]["DATA"][int_key],
													  data_2["pr"]["DATA"][int_key]
													 )

			# add to distance for every Vendor that they do not have in common and subtratc distance for how mand vendors they do have in common
			# dst += len(dt.dicts_keys_symmetric_difference(data_1["pr"]["DATA"][int_key], data_2["pr"]["DATA"][int_key])) * BITS_DATA[int_key][2]
			# print(len(dt.dicts_keys_symmetric_difference(data_1["pr"]["DATA"][int_key], data_2["pr"]["DATA"][int_key])) * BITS_DATA[int_key][2]) ########################################### DEBUG PRINT
			dst += ( len(dt.dicts_keys_symmetric_difference(data_1["pr"]["DATA"][int_key], data_2["pr"]["DATA"][int_key])) - len(same_vendors) ) * BITS_DATA[int_key][2]
			# print( ( len(dt.dicts_keys_symmetric_difference(data_1["pr"]["DATA"][int_key], data_2["pr"]["DATA"][int_key])) - len(same_vendors) ) * BITS_DATA[int_key][2]) ########################################### DEBUG PRINT
			
			for same_vendor in same_vendors:
				# find intersecting keys for data type of one vendor
				data_types = dt.dicts_keys_intersection(
														data_1["pr"]["DATA"][int_key][same_vendor],
														data_2["pr"]["DATA"][int_key][same_vendor]
													   )
				for data_type in data_types:
					dst_tmp = dt.bit_dist(
										int(data_1["pr"]["DATA"][int_key][same_vendor][data_type],16),
										int(data_2["pr"]["DATA"][int_key][same_vendor][data_type],16),
										BITS_DATA[int_key][0],
										BITS_DATA[int_key][1](data_1["pr"]["DATA"][int_key][same_vendor][data_type])
									  )
					# add calculated distance to total distance and subtract from total distance for each data_type that they have in common
					dst = dst + dst_tmp - BITS_DATA[int_key][3]
					# print(f'VENDOR : {dst_tmp}')
					sep_dsts[int_key] = dst_tmp


		elif int_key==EXT_TAG_KEY:
			dst += INCLUDE_SPECIAL_IE_SCALE
			# print(int_key)  ########################################### DEBUG PRINT
			# print("INCLUDE_SPECIAL_IE_SCALE")  ########################################### DEBUG PRINT

			# find intersecting keys for data type
			data_types = dt.dicts_keys_intersection(
													data_1["pr"]["DATA"][int_key], 
													data_2["pr"]["DATA"][int_key]
													)

			# add distance for every data type that they do not have in common
			dst += len(dt.dicts_keys_symmetric_difference(data_1["pr"]["DATA"][int_key], data_2["pr"]["DATA"][int_key])) * BITS_DATA[int_key][2]
			# print(len(dt.dicts_keys_symmetric_difference(data_1["pr"]["DATA"][int_key], data_2["pr"]["DATA"][int_key])) * BITS_DATA[int_key][2])  ########################################### DEBUG PRINT
			# dst += ( len(dt.dicts_keys_symmetric_difference(data_1["pr"]["DATA"][int_key], data_2["pr"]["DATA"][int_key])) - len(data_types) ) * BITS_DATA[int_key][2]
			


			
			for data_type in data_types:
				dst_tmp = dt.bit_dist(
									int(data_1["pr"]["DATA"][int_key][data_type],16),
									int(data_2["pr"]["DATA"][int_key][data_type],16),
									BITS_DATA[int_key][0],
									BITS_DATA[int_key][1](data_1["pr"]["DATA"][int_key][data_type])
									)
				# add calculated distance to total distance and subtract from total distance for each data_type that they have in common
				dst = dst + dst_tmp - BITS_DATA[int_key][3]
				# print(f'EXT_TAG : {dst_tmp}')
				sep_dsts[int_key] = dst_tmp

		else:
			pass

	# find min( d("TIME") + d("RSSI") ) -> indexes of comapred time must be the same as for RSSI(take into consideration true packets)
	# loop through all possible combinations of comparing each time_1 with time_2 and corresponding RSSI values
	# "TIME_1":  [ 1, 2,3,4,5]
	#			  /|\ 	... and so on and on for 2, 3, 4, 5
	#			 / | \ 	
	# "TIME_2":[1, 2,3]
	rssi_and_t_dst = float("inf")
	for i in range(0, len(data_1["pr"]["TIME"])): # it does not matter if "TIME" or "RSSI" (both are the same lengths)
		for j in range(0, len(data_2["pr"]["TIME"])):
			d = dt.RSSI_dist_pot(int(data_1["pr"]["RSSI"][i]), int(data_2["pr"]["RSSI"][j]),RSSI_K) + \
				dt.timing_dist(float(data_1["pr"]["TIME"][i]), float(data_2["pr"]["TIME"][j]),TIMING_KNEE,TIMING_SCALE,TIMING_OFFSET)
			if d < rssi_and_t_dst:
				rssi_and_t_dst = d
	
	dst += rssi_and_t_dst
	# print(f"TIME and RSSI : {rssi_and_t_dst}")
	sep_dsts['TIME and RSSI'] = rssi_and_t_dst


	# for every SSID in common lower the distance
	dst_tmp = len(dt.lists_intersection(data_1["SSID"], data_2["SSID"])) * SSID_SCALE_SAME
	dst += dst_tmp
	# print(f'SSID: {dst_tmp}')
	sep_dsts['SSID'] = dst_tmp


    # add distance for IE of symmetric difference
	dst_tmp = ( len(dt.dicts_keys_symmetric_difference(data_1["pr"]["DATA"], data_2["pr"]["DATA"]))/len(intersecting_keys) ) * NO_IE_SCALE # calculate: (intersecting_key+differing_keys)/intersecting_keys and scale it with NO_IE_SCALE
	dst += dst_tmp
	# print(f'No IE: {dst_tmp}') ########################################### DEBUG PRINT
	sep_dsts['No_IE'] = dst_tmp


	sep_dsts['DST'] = dst
	# print(sep_dsts) ########################################### DEBUG PRINT


    # OPTIONAL TODO: to bit distance you can add mask argument to specify which bits are relevant

	if dst < MIN_DST:
		dst = MIN_DST

	# print("################################")  ########################################### DEBUG PRINT
	return dst


'''
@brief Calculate "distance" between two MAC addresses
@param MAC_1 Probe requests and SSIDs of MAC_1 
@param MAC_2 Probe requests and SSIDs of MAC_2
@return "distance"
'''
def dist_btwn_two_MACs(mac_1, mac_2, func):
    # calculate "distance" for each probe request from MAC_1 to each probe request from MAC_2
    # return min of all calculated distances
    
	# store all calculated distances
	distances = []

	# store SSIDs for mac_1 and mac_2
	mac_1_ssids = []
	if 'SSID' in mac_1:
		mac_1_ssids = mac_1["SSID"]
	mac_2_ssids = []
	if 'SSID' in mac_2:
		mac_2_ssids = mac_2["SSID"]

	for pr_1 in mac_1["PROBE_REQs"]:
		for pr_2 in mac_2["PROBE_REQs"]:
			distances.append(dist_btwn_two_probe_req({"SSID": mac_1_ssids,"pr": pr_1}, {"SSID": mac_2_ssids,"pr": pr_2}))

	return func(distances)


        
def print_matrix(matrix, row_len, col_len):
	print("[")
	for i in range(0,row_len):
		for ii in range(0,col_len):
			# print('%12.2f   ' % (matrix[i][ii]), end="")
			if ii == (col_len-1):
				print(f'{matrix[i][ii]:12.2f}', end="")
			else:
				print(f'{matrix[i][ii]:12.2f},   ', end="")
		print("")
	print("]")  

def Min_2(array):
	if len(array)==0:
		return []
	elif len(array)==1:
		return [ [array[0],0] ] 
	else:
		if array[0]<array[1]:
			min1 = array[0]
			index_min_1 = 0 
			min2 = array[1]
			index_min_2 = 1
		else:
			min1 = array[1]
			index_min_1 = 1
			min2 = array[0]
			index_min_2 = 0
		for i in range(2, len(array)):
			if array[i]<min1:
				min2 = min1
				index_min_2 = index_min_1 
				min1 = array[i]
				index_min_1 = i
			elif array[i]<min2:
				min2 = array[i]
				index_min_2 = i
		return [ [min1,index_min_1] , [min2,index_min_2] ]

def optics_clustering(reach, xi_up, xi_down, epsMax):
	num_of_data = len(reach)
	labels = [-1]*num_of_data
	i=0
	in_cluster = False
	curr_label = 0
	# up_times = 0
	climbing = False
	falling = False
	while i<num_of_data-1:
		if in_cluster==True:
			if ( reach[i+1]>reach[i] ):
				if climbing==False:				
					value_start_climbing = reach[i]
					climbing = True

				if ( ((reach[i+1]/value_start_climbing) > xi_up) or (reach[i+1]>epsMax )):
				# 	# to wide gap
					in_cluster = False
					curr_label += 1
					climbing = False		
				else:
					labels[i+1] = curr_label
			else:
				climbing = False
				labels[i+1] = curr_label
		# elif (reach[i+1] < epsMax) and (reach[i+1]/reach[i]<xi_down):
		# 	labels[i] = labels[i+1] = curr_label
		# 	#i += 1
		# 	in_cluster = True
		elif (reach[i+1]<reach[i]):
			if falling==False:
				value_start_falling = reach[i]
				index_start_falling = i
				falling = True	
			if (reach[i+1] < epsMax) and (reach[i+1]/value_start_falling<xi_down):
				for ii in range(i+1, index_start_falling-1, -1):
					labels[ii] = curr_label
				#i += 1
				in_cluster = True
				falling = False
		else:
			falling = False
		i+=1
	return labels

	



data = [{
        "dataStreamId": 25,
        "payload": {
            "type": "wifi",
            "scanned_device": 
[{
    'MAC': 'a1:50:46:3a:cf:6b',
    'PROBE_REQs': [{
        'TIME': ['1643034047.856378619', '1643034058.978156284'],
        'RSSI': ['-35', '-44'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22'],
            'EXT_DATA_RTS': ['12', '18', '24', '36', '48', '72', '96', '108'],
            'HT_CAP': '0xad0113ffff000000000000000000000000000000000000000000',
            'EXT_CAP': '0x04000a02004000408001',
            'VHT_CAP': '0x92f99133faff0c03faff0c03',
            'VENDOR_SPEC': {
                '506f9a': {
                    '22': '0x030103650101'
                }
            },
            'EXT_TAG': {
                '2': '0x001c'
            }
        }
    }],
    },
	{
    'MAC': 'a2:50:46:3a:cf:6b',
    'PROBE_REQs': [{
        'TIME': ['1643034047.856378619', '1643034058.978156284'],
        'RSSI': ['-35', '-44'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22'],
            'EXT_DATA_RTS': ['12', '18', '24', '36', '48', '72', '96', '108'],
            'HT_CAP': '0xad0113ffff000000000000000000000000000000000000000000',
            'EXT_CAP': '0x04000a02004000408001',
            'VHT_CAP': '0x92f99133faff0c03faff0c03',
            'VENDOR_SPEC': {
                '506f9a': {
                    '22': '0x030103650101'
                }
            },
            'EXT_TAG': {
                '2': '0x001c'
            }
        }
    }],
    },
	{
    'MAC': 'a2:50:46:3a:cf:6b',
    'PROBE_REQs': [{
        'TIME': ['1643034047.856378619', '1643034058.978156284'],
        'RSSI': ['-35', '-44'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22'],
            'EXT_DATA_RTS': ['12', '18', '24', '36', '48', '72', '96', '108'],
            'HT_CAP': '0xad0113ffff000000000000000000000000000000000000000000',
            'EXT_CAP': '0x04000a02004000408001',
            'VHT_CAP': '0x92f99133faff0c03faff0c03',
            'VENDOR_SPEC': {
                '506f9a': {
                    '22': '0x030103650101'
                }
            },
            'EXT_TAG': {
                '2': '0x0008'
            }
        }
    }],
    },
    {
    'MAC': 'a2:50:46:3a:cf:6b',
    'PROBE_REQs': [{
        'TIME': ['1643034047.856378619', '1643034058.978156284'],
        'RSSI': ['-35', '-44'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22'],
            'EXT_DATA_RTS': ['12', '18', '24', '36', '48', '72', '96', '108'],
            'HT_CAP': '0xad0113ffff000000000000000000000000000000000000000000',
            'EXT_CAP': '0x04000a02004000408001',
            'VHT_CAP': '0x92f99133faff0c03faff0c03',
            'VENDOR_SPEC': {
                '506f9a': {
                    '22': '0x030103650101'
                }
            },
            'EXT_TAG': {
                '2': '0x001c'
            }
        }
    }, 
    {
        'TIME': ['1643034047.877195423', '1643034048.044572368', '1643034057.683585991', '1643034058.998958735', '1643034059.171656965'],
        'RSSI': ['-35', '-37', '-45', '-45', '-45'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22'],
            'EXT_DATA_RTS': ['12', '18', '24', '36', '48', '72', '96', '108'],
            'HT_CAP': '0xad0113ffff000000000000000000000000000000000000000000',
            'EXT_CAP': '0x04000a02004000408001',
            'VHT_CAP': '0x92f99133faff0c03faff0c03',
            'VENDOR_SPEC': {
                '506f9a': {
                    '22': '0x030103650101'
                }
            },
            'EXT_TAG': {
                '2': '0x0008'
            }
        }
    }, {
        'TIME': ['1643034072.901004557', '1643034072.901607954', '1643034072.905368465', '1643034072.905954248', '1643034072.906506310', '1643034072.922303771', '1643034072.923939271', '1643034072.925658353', '1643034072.927427291', '1643034072.929012281', '1643034073.029153991', '1643034073.030745882', '1643034097.680236470', '1643034097.680884538', '1643034097.688743440', '1643034097.701442891', '1643034097.703092924', '1643034097.704737623', '1643034097.706575080', '1643034097.708196081', '1643034097.803520883', '1643034097.806825586', '1643034097.808546754', '1643034097.810168029', '1643034143.376591652', '1643034143.377257748', '1643034143.377769795', '1643034143.378354558', '1643034143.379261066', '1643034143.380910333', '1643034143.382562731', '1643034143.384777594', '1643034143.386628504', '1643034143.388232849', '1643034143.474131251', '1643034143.476031014', '1643034143.477654918'],
        'RSSI': ['-42', '-44', '-43', '-42', '-42', '-44', '-43', '-45', '-43', '-42', '-44', '-45', '-42', '-44', '-42', '-42', '-43', '-45', '-44', '-43', '-45', '-44', '-45', '-45', '-42', '-43', '-42', '-42', '-41', '-43', '-43', '-44', '-43', '-44', '-44', '-44', '-45'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22'],
            'EXT_DATA_RTS': ['12', '18', '24', '36', '48', '72', '96', '108'],
            'HT_CAP': '0xad0113ffff000000000000000000000000000000000000000000',
            'EXT_CAP': '0x00000a02004000000001',
            'VHT_CAP': '0x92f99133faff0c03faff0c03',
            'VENDOR_SPEC': {
                '0050f2': {
                    '8': '0x002600'
                },
                '506f9a': {
                    '22': '0x030103650101'
                },
                '8cfdf0': {
                    '1': '0x01020100'
                }
            },
            'EXT_TAG': {
                '2': '0x00ff'
            }
        }
    }],
    'SSID': ['Doma', 'FE-GUEST', 'LTFE-guest', 'BgAAAOfWqoUAHwESRedmi 4A', 'LTFE']
 },
#  , {
#     'MAC': 'b2:52:ef:9a:0c:df',
#     'PROBE_REQs': [{
#         'TIME': ['1643034052.343801410', '1643034052.344382724', '1643034052.344952551', '1643034052.345575529', '1643034052.346119140', '1643034052.365294044', '1643034052.366937499', '1643034052.368755504', '1643034052.370520671', '1643034052.372771398', '1643034052.407445211', '1643034052.409126998', '1643034052.410785719', '1643034052.412544011', '1643034052.414143509'],
#         'RSSI': ['-40', '-41', '-40', '-40', '-39', '-42', '-40', '-41', '-40', '-41', '-43', '-43', '-43', '-44', '-43'],
#         'DATA': {
#             'SUPP_DATA_RTS': ['2', '4', '11', '22'],
#             'EXT_DATA_RTS': ['12', '18', '24', '36', '48', '72', '96', '108'],
#             'HT_CAP': '0xad0113ffff000000000000000000000000000000000000000000',
#             'EXT_CAP': '0x00000a02004000000001',
#             'VHT_CAP': '0x92f99133faff0c03faff0c03',
#             'VENDOR_SPEC': {
#                 '0050f2': {
#                     '8': '0x001f00'
#                 },
#                 '506f9a': {
#                     '22': '0x030103650101'
#                 },
#                 '8cfdf0': {
#                     '1': '0x01020100'
#                 }
#             },
#             'EXT_TAG': {
#                 '2': '0x00ff'
#             }
#         }
#     }],
#     'SSID': ['FE-GUEST', 'LTFE-guest', 'BgAAAOfWqoUAHwESRedmi 4A', 'LTFE']
# }, 
    {
    'MAC': 'd2:7e:35:b4:9d:a3',
    'PROBE_REQs': [{
        'TIME': ['1643034067.997248060', '1643034067.998475337', '1643034067.999714692', '1643034088.046355881', '1643034088.047549953', '1643034108.052092111'],
        'RSSI': ['-76', '-76', '-76', '-74', '-75', '-74'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22', '12', '18', '24', '36'],
            'HT_CAP': '0x6e0917ff00000000000000000000000000000000000000000000',
            'EXT_DATA_RTS': ['48', '72', '96', '108'],
            'INTWORK': '0x000208ffffffffffff',
            'EXT_CAP': '0x0000008001400040',
            'VHT_CAP': '0x21718003feff0000feff0000'
        }
    }],
    'SSID': ['Doma']
}, {
    'MAC': 'd2:7e:35:b4:9d:a4',
    'PROBE_REQs': [{
        'TIME': ['1643034067.997248060', '1643034067.998475337', '1643034067.999714692', '1643034088.046355881', '1643034088.047549953', '1643034108.052092111'],
        'RSSI': ['-76', '-76', '-76', '-74', '-75', '-74'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22', '12', '18', '24', '36'],
            'HT_CAP': '0x6e0917ff00000000000000000000000000000000000000000000',
            'EXT_DATA_RTS': ['48', '72', '96', '108'],
            'INTWORK': '0x000208ffffffffffff',
            'EXT_CAP': '0x0000008001400040',
            'VHT_CAP': '0x21718003feff0000feff0000'
        }
    }],
    'SSID': ['Doma']
}, {
    'MAC': '32:c9:3d:16:7f:40',
    'PROBE_REQs': [{
        'TIME': ['1643034067.997248060', '1643034067.998475337', '1643034067.999714692', '1643034088.046355881', '1643034088.047549953', '1643034108.052092111'],
        'RSSI': ['-76', '-76', '-76', '-74', '-75', '-74'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22', '12', '18', '24', '36'],
            'HT_CAP': '0x6e0917ff00000000000000000000000000000000000000000000',
            'EXT_DATA_RTS': ['48', '72', '96', '108'],
            'INTWORK': '0x000208ffffffffffff',
            'EXT_CAP': '0x0000008001400040',
            'VHT_CAP': '0x21718003feff0000feff0000'
        }
    }],
    'SSID': ['Doma']
}, {
    'MAC': '32:c9:3d:16:7f:41',
    'PROBE_REQs': [{
        'TIME': ['1643034067.997248060', '1643034067.998475337', '1643034067.999714692', '1643034088.046355881', '1643034088.047549953', '1643034108.052092111'],
        'RSSI': ['-76', '-76', '-76', '-74', '-75', '-74'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22', '12', '18', '24', '36'],
            'HT_CAP': '0x6e0917ff00000000000000000000000000000000000000000000',
            'EXT_DATA_RTS': ['48', '72', '96', '108'],
            'INTWORK': '0x000208ffffffffffff',
            'EXT_CAP': '0x0000008001400040',
            'VHT_CAP': '0x21718003feff0000feff0000'
        }
    }],
    'SSID': ['Doma']
}, 
{
    'MAC': '32:c9:3d:16:7f:42',
    'PROBE_REQs': [{
        'TIME': ['1643034095.746599770'],
        'RSSI': ['-92'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22', '12', '18', '24', '36'],
            'EXT_DATA_RTS': ['48', '72', '96', '108'],
            'HT_CAP': '0xe70917ffff000000000000000000000000000000000000000000',
            'EXT_CAP': '0x0400c880014000000000',
            'VENDOR_SPEC': {
                '506f9a': {
                    '22': '0x030103'
                }
            },
            'EXT_TAG': {
                '35': '0x0178200ac0ab0e300e00fd098c0e0ff200fafffafffafffaff611cc771'
            }
        }
    }]
}, {
    'MAC': '32:c9:3d:16:7f:43',
    'PROBE_REQs': [{
        'TIME': ['1643034095.746599770'],
        'RSSI': ['-92'],
        'DATA': {
            'SUPP_DATA_RTS': ['2', '4', '11', '22', '12', '18', '24', '36'],
            'EXT_DATA_RTS': ['48', '72', '96', '108'],
            'HT_CAP': '0xe70917ffff000000000000000000000000000000000000000000',
            'EXT_CAP': '0x0400c880014000000000',
            'VENDOR_SPEC': {
                '506f9a': {
                    '22': '0x030103'
                }
            },
            'EXT_TAG': {
                '35': '0x0178200ac0ab0e300e00fd098c0e0ff200fafffafffafffaff611cc771'
            }
        }
    }]
}
],
            "avg_device_number": 2,
            "max_device_number": 2,
            "start_timestamp": 1631884957,
            "end_timestamp": 1631884967,
            "device": "resiloc_s11"
        },
        "timestamp": "2021-09-17 15:22:55",
    }

]

import json 
# data_file = open('sorted_data_test_medium_get.json', 'r')
# data_file = open('sorted_data_test_short_get.json', 'r')
data_file = open('sorted_data_new_get.json', 'r')
data = json.load(data_file)
data_file.close()


print(f"Data analysis for device: {args.device}")
# {"dataStreamId": 38, 
# "payload": {
# 	"type": "wifi", 
# 	"scanned_device": {"2c:9f:fb:cb:65:5c": -82, "10:0b:a9:19:15:40": -82}, 
# 	"start_timestamp": 1643363353, 
# 	"end_timestamp": 1643363363, 
# 	"device": "Latitude: 46.0455936 Longitude: 14.4867328 Accuracy: 1146.6127349598546\n", 
# 	"serial": "000000007a69c602"}, 
# "timestamp": "2022-01-28 10:49:34"}

# gather all probe request from different scan slots and sort them according to MAC address
all_probe_reqs = []
for scan_slot in data:
		if scan_slot["payload"]["serial"]==args.device:
			all_probe_reqs.extend(scan_slot["payload"]["scanned_device"])



probe_reqs = [i for i in sorted(all_probe_reqs, key=lambda device: device["MAC"]) ]
########################### OLD DATA STRUCTURE ############################################# 
# probe_reqs = [{"mac":i, "data":data[0]["payload"]["scanned_device"][i]} for i in sorted(data[0]["payload"]["scanned_device"] )]
# probe_reqs = [i for i in sorted(data[0]["payload"]["scanned_device"] )]

# pp.pprint(probe_reqs)


# pr_N={ 
            #       'TIME': [X], 
            #       'RSSI': [X], 
            #       'DATA':{
            #           'SUPP_DATA_RTS': X, 
            #           'EXT_DATA_RTS': X, 
            #           'HT_CAP': X, 
            #           'EXT_CAP': {'length':X, 'data':Y}, 
            #           'VHT_CAP': X, 
            #           'INTERWORKING': X, 
            #           'EXT_TAG': {'ID_1': DATA_1, 'ID_2': DATA_2,..}, 
            #           'VENDOR_SPEC': {VENDOR_1:{'ID_1': DATA_1, 'ID_2': DATA_2,..}, VENDOR_2:{'ID_1': DATA_1, 'ID_2': DATA_2,..}} } }
            # 
            # {'MAC': mac_1, 'SSIDs': [SSID_1, SSID_2,..], 'PROBE_REQs': [pr_1, pr_2,...] }
            
            # tmp_devices_prs = copy.deepcopy(self._devices[mac][PROBE_REQs_KEY])


#######################################         STEP 1         #######################################
# categorize probe requests in the clusters (seperate cluster for global MAC addresses each OUI found and seperate cluster for completely random MAC addresses)
# TODO DONE: gateher all SSIDs gathered from probe requests for one MAC address in single structure. For example: { 	"{mac_1}": {"SSIDs": [SSID_1, SSID_2,...], "pr": [probe_reqs] }, "{mac_2}": {"SSIDs": [SSID_1, SSID_2,...], "pr": [probe_reqs] } ,...   })


# global_devices = dict() # { 	"{mac_1}": [probe_reqs], "{mac_2}": [probe_reqs] ,...   } # OLD STRUCTURE
global_devices = list() # [ {prs(mac_1)},  {prs(mac_2)}, ...   ]
random_devices = dict() # { 	"{oui_1}": { "{mac_1}": [probe_reqs], "{mac_2}": [probe_reqs], ... },		 "{oui_2}": { "{mac_1}": [probe_reqs], "{mac_2}": [probe_reqs], ... },    "random": { "{mac_1}": [probe_reqs], "{mac_2}": [probe_reqs], ... }    }
random_devices["random"] = dict()


if len(probe_reqs) > 0:
	# # global_devices = dict() # { 	"{mac_1}": [probe_reqs], "{mac_2}": [probe_reqs] ,...   } # OLD STRUCTURE
	# global_devices = list() # [ {prs(mac_1)},  {prs(mac_2)}, ...   ]
	# random_devices = dict() # { 	"{oui_1}": { "{mac_1}": [probe_reqs], "{mac_2}": [probe_reqs], ... },		 "{oui_2}": { "{mac_1}": [probe_reqs], "{mac_2}": [probe_reqs], ... },    "random": { "{mac_1}": [probe_reqs], "{mac_2}": [probe_reqs], ... }    }
	# random_devices["random"] = dict()

	in_oui = 0;
	reqs = []
	i = 0
	new_address = 0
	# print(len(probe_reqs))
	while ( i < (len(probe_reqs)-1) ):

	# stumbled on new address

		# loop through all MAC addresses that are the same and append probe requests to list
		while (new_address==0) and (i < (len(probe_reqs)-1)):
			# global i
			reqs.append(probe_reqs[i])
			
			# print(probe_reqs[i]["MAC"])
			# print(probe_reqs[i+1]["MAC"])

			# if next MAC address is the same continue in the while loop
			if (  probe_reqs[i]["MAC"] == probe_reqs[i+1]["MAC"]  ): 
				# print("Same address")

				# if (i==(len(probe_reqs)-2)):
				# 	if in_oui==1:
				# 		random_devices[probe_reqs[i]["mac"][0:8]] [probe_reqs[i]["mac"]] = reqs
				# 	else:
				# 		random_devices['random'] [probe_reqs[i]["mac"]] = reqs

					# print(probe_reqs[i])
				i = i + 1
			# if next address is different -> categorize probe requests of one MAC address collected in loop
			else:
				# merge all probe requests and SSIDs from one MAC address in reqs to 0-th element
				if len(reqs) > 1:
					SSIDs = []
					if 'SSID' in reqs[0]:
						SSIDs.append(reqs[0]['SSID'])
					
					for i_req in range(1,len(reqs)):

						different_probe_req_buffer = []
						for pr_reqs_i in reqs[i_req]['PROBE_REQs']:
							found_same_probe_req = False
							for pr_reqs_0 in reqs[0]['PROBE_REQs']:
								if pr_reqs_i["DATA"]==pr_reqs_0["DATA"]:
									pr_reqs_0["TIME"].extend(pr_reqs_i["TIME"])
									pr_reqs_0["RSSI"].extend(pr_reqs_i["RSSI"])
									found_same_probe_req = True
									break
							if found_same_probe_req == False:
								different_probe_req_buffer.append(pr_reqs_i)
						
						reqs[0]['PROBE_REQs'].extend(different_probe_req_buffer)

						# check if i_req-th even has SSID key (PROBE_REQs key is there by default even it is empyt)
						if 'SSID' in reqs[i_req]:
							SSIDs.append(reqs[i_req]['SSID'])
					
					if len(SSIDs) > 0:
						reqs[0]['SSID'] = dt.union_of_lists_wo_repetitions(SSIDs)

				# print("New address")
				new_address = 1

				# if probe requests of colcted MAC address is global
				if probe_reqs[i]["MAC"][1] not in ['2','6','a','e' ]:
					# global_devices[probe_reqs[i]["MAC"]] = reqs[0] # FOR OLD STRUCTURE
					global_devices.append(reqs[0])
					in_oui = 0

				# if not global
				else:
					# if I already know that I am part of some OUI add me to this 
					if in_oui==1:
						random_devices[probe_reqs[i]["MAC"][0:8]] [probe_reqs[i]["MAC"]] = reqs[0]
						if not (  probe_reqs[i]["MAC"][0:8] == probe_reqs[i+1]["MAC"][0:8]  ):
							in_oui = 0
					# if I do not know of OUI (first occurence in list of some MAC addres) than I need to check if next address can be part of same OUI
					else:
					# if first three Bytes of next address is the same -> it is part of the same OUI
						if (  probe_reqs[i]["MAC"][0:8] == probe_reqs[i+1]["MAC"][0:8]  ):
							in_oui = 1
							# create new OUI in radnom devices and add apenneded requests to that oui
							random_devices[probe_reqs[i]["MAC"][0:8]] = dict()
							random_devices[probe_reqs[i]["MAC"][0:8]] [probe_reqs[i]["MAC"]] = reqs[0]
						else:
							# add it to random 
							random_devices['random'] [probe_reqs[i]["MAC"]] = reqs[0]
							in_oui = 0
							
				i = i + 1
				reqs = []
						

		new_address = 0


	# categorize last element or lasts elements of same MAC address of probe requests
	# if (  probe_reqs[i]["mac"] == probe_reqs[i-1]["mac"] ):
	# 	if in_oui==1 : 
	# 		random_devices[probe_reqs[i]["mac"][0:8]] [probe_reqs[i]["mac"]].append(probe_reqs[i])
	# 	else:
	# 		random_devices['random'] [probe_reqs[i]["mac"]].append(probe_reqs[i])
	if (len(reqs)>=1):
		reqs.append(probe_reqs[i])

		SSIDs = []
		if 'SSID' in reqs[0]:
			SSIDs.append(reqs[0]['SSID'])

		for i_req in range(1,len(reqs)):

			different_probe_req_buffer = []
			for pr_reqs_i in reqs[i_req]['PROBE_REQs']:
				found_same_probe_req = False
				for pr_reqs_0 in reqs[0]['PROBE_REQs']:
					if pr_reqs_i["DATA"]==pr_reqs_0["DATA"]:
						pr_reqs_0["TIME"].extend(pr_reqs_i["TIME"])
						pr_reqs_0["RSSI"].extend(pr_reqs_i["RSSI"])
						found_same_probe_req = True
						break
				if found_same_probe_req == False:
					different_probe_req_buffer.append(pr_reqs_i)
			
			reqs[0]['PROBE_REQs'].extend(different_probe_req_buffer)

			# check if i_req-th even has SSID key (PROBE_REQs key is there by default even it is empyt)
			if 'SSID' in reqs[i_req]:
				SSIDs.append(reqs[i_req]['SSID'])

		if len(SSIDs) > 0:
			reqs[0]['SSID'] = dt.union_of_lists_wo_repetitions(SSIDs)

		if in_oui==1:
			random_devices[probe_reqs[i]["MAC"][0:8]] [probe_reqs[i]["MAC"]] = reqs[0]
		else:
			random_devices['random'] [probe_reqs[i]["MAC"]] = reqs[0]
	elif in_oui==1:
		random_devices[probe_reqs[i]["MAC"][0:8]] [probe_reqs[i]["MAC"]] = probe_reqs[i]
	elif probe_reqs[i]["MAC"][1] not in ['2','6','a','e' ]:
		# global_devices[probe_reqs[i]["MAC"]] = probe_reqs[i] # FOR OLD DATA STRUCTURE
		global_devices.append(probe_reqs[i])
	else:
		random_devices['random'] [probe_reqs[i]["MAC"]] = probe_reqs[i]

	# print("Global devices: ")
	# pp.pprint(global_devices)
	# print("Random devices: ")
	# pp.pprint(random_devices)




	#######################################         STEP 2         #######################################
	# in each cluster run algorithm to cluster MACs to single devices. For example OPTICS. 
	# distance between two MACs with more probe request is min distance of calculated distances between each probe request from MAC 1 to each probe request from MAC 2
	# alternatively to min distance you can choose max distance (I dont se a point here), average distance (not much of a point eaiher)
	# alternatively to OPTICS algorith you can 


	clusters = [[]]
	plt.figure(figsize=(10, 7))

	for stevec,cluster in enumerate(random_devices):
		print(f'Calculating distances for cluster: {cluster}')
		num_of_macs = len(random_devices[cluster])
		macs = list(random_devices[cluster])

		# calculate distances for each mac with each mac
			# Initialize two dimensional array
			# X = [[0 for j in range(0,len(probe_reqs))] for i in range(0,len(probe_reqs))]
		X = [x[:] for x in [[0] * num_of_macs] * num_of_macs] # faster one than the one above
		for i in range(0,num_of_macs):
			for ii in range(i,num_of_macs):
				# X[i].insert(ii, dist_btwn_two_probe_req(probe_reqs[i], probe_reqs[ii]))
				# X[i][ii] = X[ii][i] = dist_btwn_two_MACs(random_devices[cluster][macs[i]], random_devices[cluster][macs[ii]], lambda x:min(x))
				X[i][ii] = X[ii][i] = dist_btwn_two_MACs(random_devices[cluster][macs[i]], random_devices[cluster][macs[ii]], lambda x: sum( sorted(x)[ 0:math.ceil(len(x)/3) ] ) / float(len(x[ 0:math.ceil(len(x)/3) ]))        )
				

				
				
		# print('\n'*2)
		# print_matrix(X,num_of_macs,num_of_macs)
		

		if len(X) > 1: # cant do clustering with only one device!
			X = np.array(X, float)

			#clust = OPTICS(min_samples=50, xi=0.05, min_cluster_size=0.05)
			clust = OPTICS(min_samples=MIN_SAMPLES_ST_1, 
							min_cluster_size=MIN_CLUSTER_SIZE_ST_1, 
							max_eps=MAX_EPS_ST_1, 
							cluster_method=CLUSTER_METHOD_ST_1, 
							xi=XI_ST_1, 
							eps=EPS_ST_1, 
							metric='precomputed',
							n_jobs=-1)

			# Run the fit
			clust.fit(X)

			# OPTICS data
			reachability = clust.reachability_[clust.ordering_]
			print(reachability)
			# print(f'reachability = {reachability}')
			# labels = clust.labels_[clust.ordering_] # scikit clustering algorithm
			# print(labels) # scikit clustering algorithm
			labels = optics_clustering(reachability, XI_UP, XI_DOWN, MAX_EPS_ST_1)
			labels = np.array(labels, np.int64)
			print(np.array2string(labels, separator=', '))
			# print(f'labels = {labels}')
			# print(f'Ordering_ = {clust.ordering_}')
			# print(f'Core distances = {clust.core_distances_[clust.ordering_]}')
			# print(f'predecessor = {clust.predecessor_[clust.ordering_]}')


			if PLOT_GRAPHICS:
				# Reachability plot
				G = gridspec.GridSpec(len(random_devices),1)
				ax1 = plt.subplot(G[stevec, :])
				# ax1 = plt.plot()
				# ax2 = plt.subplot(G[1, 0])
				# ax3 = plt.subplot(G[1, 1])
				# ax4 = plt.subplot(G[1, 2])

				space = np.arange(len(random_devices[cluster]))
				colors = ["brown", "lime", "red", "cyan", "peru", "greenyellow", "orange", "navy", "coral", "gold", "lawngreen", "darkslategrey", "pink", "dodgerblue", "magenta", "mediumvioletred"]
				for klass, color in zip(range(0, max(labels)+1), colors[0:(max(labels)+1)]):
					Xk = space[labels == klass]
					Rk = reachability[labels == klass]
					ax1.plot(Xk, Rk, marker='.', color=color, alpha=1)
				ax1.plot(space[labels == -1], reachability[labels == -1], marker='.', color="black", alpha=0.8, linestyle = 'None')
				# plt.plot(space, np.full_like(space, 2.0, dtype=float), "k-", alpha=0.5)
				# plt.plot(space, np.full_like(space, 0.5, dtype=float), "k-.", alpha=0.5)
				ax1.set_ylabel("Reachability (epsilon distance)")
				# ax1.set_ylim([0,3000])
				ax1.set_title(f'Reachability Plot {cluster}')

				# plt.show()


			# add found clusters to global clusters
			# append number of empty lists [] /empty directorys corresponding to max number in labels (number of clusters is max(label)+1)
			# clusters[0] is for unclustered devices
			#[[],   {'Merged': {}, 'Seperated': []},      {'Merged': {}, 'Seperated': []},      {'Merged': {}, 'Seperated': []}]
			clusters.extend( [{"Seperated":[]} for _ in range(0,max(labels)+1)] )
			# save labels that are unordered (order is the same as input to distances calculation so it is the same as order of sorted probe requests)
			# unordered_labels = clust.labels_ # lebels represents clusters (-1 -> unclustered, 0 -> cluster0, 1 -> cluster1, 2 -> cluster2,...)
			unordered_labels = labels[np.argsort(clust.ordering_)]
			
			# print(random_devices[cluster].keys())
			# labels_to_macs = list(zip(cluster, labels))
			for i,mac in enumerate(random_devices[cluster]):
				# check if MAC belogs to any of clusters
				if unordered_labels[i] != -1:
					# clusters[-unordered_labels[i]-1].append(list(random_devices[cluster].keys())[i])
					
					# look for same probe requests and merge them; also merge MAC addresses
					num_of_dev_already_in_cluster = len(clusters[-unordered_labels[i]-1]["Seperated"])
					if num_of_dev_already_in_cluster==0:
						# clusters[-unordered_labels[i]-1].append(copy.deepcopy(random_devices[cluster][mac])) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
						clusters[-unordered_labels[i]-1]["Merged"] = copy.deepcopy(random_devices[cluster][mac]) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
						clusters[-unordered_labels[i]-1]["Seperated"].append(random_devices[cluster][mac]) # append one above for saving all probe requests to and this one for archiving all different probe requests from different MACs
						clusters[-unordered_labels[i]-1]["Merged"]["MAC"] = [ random_devices[cluster][mac]["MAC"] ] # make array where MACs will be stored and save MAC address of first MAC
					else:

						different_probe_req_buffer = []
						for pr_reqs_i in random_devices[cluster][mac]['PROBE_REQs']:
							found_same_probe_req = False
							for pr_reqs_0 in clusters[-unordered_labels[i]-1]["Merged"]['PROBE_REQs']:
								if pr_reqs_i["DATA"]==pr_reqs_0["DATA"]:
									pr_reqs_0["TIME"].extend(pr_reqs_i["TIME"])
									pr_reqs_0["RSSI"].extend(pr_reqs_i["RSSI"])
									found_same_probe_req = True
									break
							if found_same_probe_req == False:
								different_probe_req_buffer.append(pr_reqs_i)
						
						clusters[-unordered_labels[i]-1]["Merged"]['PROBE_REQs'].extend(different_probe_req_buffer)

						# append MAC address to MAC addresses list
						clusters[-unordered_labels[i]-1]["Merged"]["MAC"].append(random_devices[cluster][mac]["MAC"])

						# check if i_req-th even has SSID key (PROBE_REQs key is there by default even it is empyt)
						if 'SSID' in random_devices[cluster][mac]:
							if 'SSID' not in clusters[-unordered_labels[i]-1]["Merged"]:
								clusters[-unordered_labels[i]-1]["Merged"]['SSID'] = []
								clusters[-unordered_labels[i]-1]["Merged"]['SSID'].extend(random_devices[cluster][mac]['SSID'])
							else:
								clusters[-unordered_labels[i]-1]["Merged"]['SSID'] = dt.union_of_lists_wo_repetitions([
																												clusters[-unordered_labels[i]-1]["Merged"]['SSID'],
																												random_devices[cluster][mac]['SSID']
																													])
						# append device to list of seperete devices
						clusters[-unordered_labels[i]-1]["Seperated"].append(random_devices[cluster][mac]) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
				
				else: # if unordered_labels[i] != -1:
					# if it does not belong to any of formed clusters than append it to unclostered devices
					# clusters[0].append(list(random_devices[cluster].keys())[i])
					clusters[0].append(random_devices[cluster][mac])

		
		elif len(X) == 1: # len(X) > 1: # cant do clustering with only one device!
			# append this one device to unclustered list (this excpetion can not be part of any OUI since there must be at least two devices to form OUI so it must be under "random")
			clusters[0].append(random_devices[cluster][macs[0]])
		elif len(X)==0 and len(random_devices)<2: # make sure that not only random cluster is empty
			print("No random devices")
			no_random_devs = True
			# total_devices = len(global_devices) # assigned later
			

	# pp.pprint(clusters)



	if PLOT_GRAPHICS:
		plt.show()


	#######################################         STEP 3         #######################################
	# Group clusters in each OUI and random cluster with global MAC addresses
	# TWO methods:
	# 1. for each global divece calculate distance to each cluster and unclustered devices -> if distance < Threshold -> merge two devices together
	# 2. run optics algorithm on all global devices, unclustered devices and clustered devices. For all devices that can not be the same (between global devices or clustered devices) predefine distance float("inf")
	num_of_global_dev = len(global_devices) # global_devices[ {"MAC":.., "PROBE_REQs":...}, {"MAC":.., "PROBE_REQs":...} ,..)
	# global_macs = list(global_devices) # [mac1, mac2, ...] # NECESSARY FOR OLD DATA STRUCTURE
	num_of_unclustered_dev = len(clusters[0])# clusters= [[],   {'Merged': {}, 'Seperated': []}      {'Merged': {}, 'Seperated': []},      {'Merged': {}, 'Seperated': []}]
	unclustered_dev = clusters[0]
	
	num_of_clustered_dev = len(clusters) - 1 # -1 to not count clusters[0]
	clustered_dev = clusters[1:]

	num_of_all_combined = num_of_global_dev + num_of_unclustered_dev + num_of_clustered_dev
	
	clusters_final = [[]]
	
	if ( (not no_random_devs) and (num_of_global_dev >= 1) ): 
		


		# calculate distances for each mac in global with with each mac in unclustered devices (clusters[0] : list) and with each clustered device (slusters[!0]['Merged']: dict)
		# Initialize two dimensional array
		# X = [[0 for j in range(0,len(probe_reqs))] for i in range(0,len(probe_reqs))]
		X = [x[:] for x in [[10000000] * num_of_all_combined] * num_of_all_combined] # faster one than the one above # optics cant handle float("inf")
		for i in range(0,num_of_all_combined):
			for ii in range(i,num_of_all_combined):
				
				# global devices section (from columns perspective)
				if ii < num_of_global_dev:
					# calculate only distances in diagonal (maybe even this is not neccessary and it is dependent on if optics takes this distances also in account)
					if i==ii:
						X[i][ii] = X[ii][i] = dist_btwn_two_MACs(
																#  global_devices[global_macs[ii]], # can be ii since i=ii in this case # FOR OLD DATA STRUCTURE
																#  global_devices[global_macs[ii]], # FOR OLD DATA STRUCTURE
																global_devices[ii], # can be ii since i=ii in this case
																global_devices[ii],
																lambda x: sum(x)/float(len(x))
																# lambda x: sum( sorted(x)[ 0:math.ceil(len(x)/3) ] ) / float(len(x[ 0:math.ceil(len(x)/3) ]))
																)
				# unclustered and clustered devices section (from columns perspective)
				else: # (ii >= num_of_global_dev):
					
					# calculate all distances
					if (i < num_of_global_dev):
						
						# unclustered devices section
						if (ii < (num_of_global_dev+num_of_unclustered_dev)):
							X[i][ii] = X[ii][i] = dist_btwn_two_MACs(
																	# global_devices[global_macs[i]],# FOR OLD DATA STRUCTURE
																	global_devices[i],
																	unclustered_dev[ii-num_of_global_dev],
																	# lambda x: sum(x)/float(len(x))
																	lambda x: sum( sorted(x)[ 0:math.ceil(len(x)/3) ] ) / float(len(x[ 0:math.ceil(len(x)/3) ]))
																	)
						# clustered devices section
						else: #ii > (num_of_global_dev+num_of_unclustered_dev)
							X[i][ii] = X[ii][i] = dist_btwn_two_MACs(
																	# global_devices[global_macs[i]],# FOR OLD DATA STRUCTURE
																	global_devices[i],
																	clustered_dev[ii-num_of_global_dev-num_of_unclustered_dev]["Merged"],
																	# lambda x: sum(x)/float(len(x))
																	lambda x: sum( sorted(x)[ 0:math.ceil(len(x)/3) ] ) / float(len(x[ 0:math.ceil(len(x)/3) ]))
																	)

					#calculate only diagonals
					else: # (i >= num_of_global_dev)
						# unclustered devices section
						if (i==ii):
							if (ii < (num_of_global_dev+num_of_unclustered_dev)):
							
								X[i][ii] = X[ii][i] = dist_btwn_two_MACs(
																		unclustered_dev[ii-num_of_global_dev], # can be ii since i=ii in this case
																		unclustered_dev[ii-num_of_global_dev],
																		# lambda x: sum(x)/float(len(x))
																		lambda x: sum( sorted(x)[ 0:math.ceil(len(x)/3) ] ) / float(len(x[ 0:math.ceil(len(x)/3) ]))
																		)

							else: # (ii >= (num_of_global_dev+num_of_unclustered_dev))
								X[i][ii] = X[ii][i] = dist_btwn_two_MACs(
																		clustered_dev[ii-num_of_global_dev-num_of_unclustered_dev]["Merged"], # can be ii since i=ii in this case
																		clustered_dev[ii-num_of_global_dev-num_of_unclustered_dev]["Merged"],
																		# lambda x: sum(x)/float(len(x))
																		lambda x: sum( sorted(x)[ 0:math.ceil(len(x)/3) ] ) / float(len(x[ 0:math.ceil(len(x)/3) ]))
																		)



		# print('\n'*2)
		print("MATRIX of second stage")
		print_matrix(X,num_of_all_combined,num_of_all_combined)
		
		if SECOND_STAGE_OPTICS == True:
			
			# if len(X) > 1: # cant do clustering with only one device!
			X = np.array(X, float)

			#clust = OPTICS(min_samples=50, xi=0.05, min_cluster_size=0.05)
			clust = OPTICS(min_samples=MIN_SAMPLES_ST_2, min_cluster_size=MIN_CLUSTER_SIZE_ST_2, max_eps=MAX_EPS_ST_2, cluster_method=CLUSTER_METHOD_ST_2, xi=XI_ST_2, eps=EPS_ST_2, metric='precomputed')


			# Run the fit
			clust.fit(X)

			# OPTICS data
			reachability = clust.reachability_[clust.ordering_]
			# print(f'reachability = {reachability}')
			labels = clust.labels_[clust.ordering_]
			# print(f'labels = {labels}')
			# print(f'Ordering_ = {clust.ordering_}')
			# print(f'Core distances = {clust.core_distances_[clust.ordering_]}')
			# print(f'predecessor = {clust.predecessor_[clust.ordering_]}')


			if PLOT_GRAPHICS:
				# Reachability plot

				G = gridspec.GridSpec(1,1)
				ax1 = plt.subplot(G[0, :])
				# ax1 = plt.plot()
				# ax2 = plt.subplot(G[1, 0])
				# ax3 = plt.subplot(G[1, 1])
				# ax4 = plt.subplot(G[1, 2])

				space = np.arange(len(X))
				colors = ["brown", "lime", "red","cyan","peru","greenyellow", "orange", "navy","coral","gold", "lawngreen", "darkslategrey", "pink", "dodgerblue" "magenta", "mediumvioletred"]
				for klass, color in zip(range(0, max(labels)), colors[0:(max(labels)+1)]):
					Xk = space[labels == klass]
					Rk = reachability[labels == klass]
					ax1.plot(Xk, Rk, marker='.', color=color, alpha=1)
				ax1.plot(space[labels == -1], reachability[labels == -1], marker='.', color="black", alpha=0.8, linestyle = 'None')
				# plt.plot(space, np.full_like(space, 2.0, dtype=float), "k-", alpha=0.5)
				# plt.plot(space, np.full_like(space, 0.5, dtype=float), "k-.", alpha=0.5)
				ax1.set_ylabel("Reachability (epsilon distance)")
				ax1.set_title(f'Reachability Plot ')
				
				plt.show()



			unordered_labels = clust.labels_ # lebels represents clusters (-1 -> unclustered, 0 -> cluster0, 1 -> cluster1, 2 -> cluster2,...)
			

			# here we can just count how many times -1 appears in unordered_labels and read max(unordered_labels)
			# total number of devices = count(-1) + max(unordered_labels)+1
			total_devices = len(unordered_labels[unordered_labels==-1]) + max(unordered_labels)+1


			# append number of empty lists [] /empty directorys corresponding to max number in labels (number of clusters is max(label)+1)
			# clusters_final[0] is for unclustered devices
			#[[],   {'Merged': {}, 'Seperated': []},      {'Merged': {}, 'Seperated': []},      {'Merged': {}, 'Seperated': []}]
			clusters_final.extend( [{"Seperated":[]} for _ in range(0,max(clust.labels_)+1)] )
			# save labels that are unordered (order is the same as input to distances calculation so it is the same as order of sorted probe requests)
			
			# labels_to_macs = list(zip(cluster, labels))
			for ii,label in enumerate(unordered_labels):

				if ii < num_of_global_dev:
					device = global_devices[ii]
				elif ii < (num_of_global_dev+num_of_unclustered_dev):
					device = unclustered_dev[ii-num_of_global_dev]
				else: # ii >= num_of_global_dev+num_of_unclustered_dev
					device = clustered_dev[ii-num_of_global_dev-num_of_unclustered_dev]["Merged"]

				# check if MAC belogs to any of clusters
				if label != -1:

					# look for same probe requests and merge them; also merge MAC addresses
					num_of_dev_already_in_cluster = len(clusters_final[-label-1]["Seperated"])
					if num_of_dev_already_in_cluster==0:

						# if ii < (num_of_global_dev+num_of_unclustered_dev):
						clusters_final[-label-1]["Merged"] = copy.deepcopy(device) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
						clusters_final[-label-1]["Seperated"].append(device) # append one above for saving all probe requests to and this one for archiving all different probe requests from different MACs
						clusters_final[-label-1]["Merged"]["MAC"] = [ device["MAC"] ] # make array where MACs will be stored and save MAC address of first MAC

						# else: # ii >= num_of_global_dev+num_of_unclustered_dev
						# 	clusters_final[-label-1]["Merged"] = copy.deepcopy(clustered_dev[ii-num_of_global_dev-num_of_unclustered_dev]["Merged"]) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
						# 	clusters_final[-label-1]["Seperated"].extend(clustered_dev[ii-num_of_global_dev-num_of_unclustered_dev]["Seperated"]) # append one above for saving all probe requests to and this one for archiving all different probe requests from different MACs
						
						
					else:

						different_probe_req_buffer = []
						for pr_reqs_i in device['PROBE_REQs']:
							found_same_probe_req = False
							for pr_reqs_0 in clusters_final[-label-1]["Merged"]['PROBE_REQs']:
								if pr_reqs_i["DATA"]==pr_reqs_0["DATA"]:
									pr_reqs_0["TIME"].extend(pr_reqs_i["TIME"])
									pr_reqs_0["RSSI"].extend(pr_reqs_i["RSSI"])
									found_same_probe_req = True
									break
							if found_same_probe_req == False:
								different_probe_req_buffer.append(pr_reqs_i)
						
						clusters_final[-label-1]["Merged"]['PROBE_REQs'].extend(different_probe_req_buffer)

						# append MAC address to MAC addresses list
						clusters_final[-label-1]["Merged"]["MAC"].append(device["MAC"])

						# check if i_req-th even has SSID key (PROBE_REQs key is there by default even it is empyt)
						if 'SSID' in device:
							if 'SSID' not in clusters_final[-label-1]["Merged"]:
								clusters_final[-label-1]["Merged"]['SSID'] = []
								clusters_final[-label-1]["Merged"]['SSID'].extend(device['SSID'])
							else:
								clusters_final[-label-1]["Merged"]['SSID'] = dt.union_of_lists_wo_repetitions([
																												clusters_final[-label-1]["Merged"]['SSID'],
																												device['SSID']
																											])
						# append device to list of seperete devices
						clusters_final[-label-1]["Seperated"].append(device) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
				
				else: # if label == -1:
					# if it does not belong to any of formed clusters than append it to unclostered devices
					clusters_final[0].append(device)

			# elif len(X)==1:
			# 	# append this device to unclustered devices
				
			# 	if len(global_devices)==1:
			# 		clusters_final[0].append(global_devices[0])
			# 	# if it is not global then it mus be random random device
			# 	else:
			# 		clusters_final[0].append(list(random_devices['random'].values())[0])

			# 	total_devices = 1 
			# 	# we got only one or zero device in our input
			# else: # len(X)==0
			# 	total_devices =  0	

			# print("CLUSTERS_FINALS")
			# pp.pprint(clusters_final)
		
		else: # NOT OPTICS
			# for every global device find two minimum distances and theri indexes that represents matching to appropriate random device
			
			clusters_final=copy.deepcopy(clusters)

			# 1 global and >=2 random devices
			if ( ((num_of_clustered_dev+num_of_unclustered_dev)>1) and (num_of_global_dev==1) ):
				print("# 1 global and >=2 random devices")
				# "matching global to random devices"
				
				mins = Min_2(X[0][num_of_global_dev:]) # find two minimums of first row in X
				
				if ( (mins[0][0]<DISTANCE_THRESHOLD) and (mins[0][0]*KOEFF_DIFF<mins[1][0]) ):
					# group two devices together
					print("Matched")
					gl_dev = global_devices[0]

					if mins[0][1] < num_of_unclustered_dev: # if index of random devices in under unclustered devices -> pop it from unclustered and add new cluster with these two devices
						random_dev = clusters_final[0].pop(mins[0][1]) # remove random device from unclustered list since it is now clustered
						clusters_final.append({"Seperated":[]})

						clusters_final[-1]["Merged"] = copy.deepcopy(random_dev) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
						clusters_final[-1]["Seperated"].append(random_dev) # append one above for saving all probe requests to and this one for archiving all different probe requests from different MACs
						clusters_final[-1]["Merged"]["MAC"] = [ random_dev["MAC"] ] # make array where MACs will be stored and save MAC address of first MAC

						index_of_random_device_in_clusters_final = -1					

					else: # index of random devices is under clustered devices
						index_of_random_device_in_clusters_final = mins[0][1]-num_of_unclustered_dev+1

					#append global device to random (check for repeating probe requests)
					different_probe_req_buffer = []
					for pr_reqs_i in gl_dev['PROBE_REQs']:
						found_same_probe_req = False
						for pr_reqs_0 in clusters_final[index_of_random_device_in_clusters_final]["Merged"]['PROBE_REQs']:
							if pr_reqs_i["DATA"]==pr_reqs_0["DATA"]:
								pr_reqs_0["TIME"].extend(pr_reqs_i["TIME"])
								pr_reqs_0["RSSI"].extend(pr_reqs_i["RSSI"])
								found_same_probe_req = True
								break
						if found_same_probe_req == False:
							different_probe_req_buffer.append(pr_reqs_i)
					
					clusters_final[index_of_random_device_in_clusters_final]["Merged"]['PROBE_REQs'].extend(different_probe_req_buffer)

					# append MAC address to MAC addresses list
					clusters_final[index_of_random_device_in_clusters_final]["Merged"]["MAC"].append(gl_dev["MAC"])

					# check if global device even has SSID key (PROBE_REQs key is there by default even it is empyt)
					if 'SSID' in gl_dev:
						if 'SSID' not in clusters_final[index_of_random_device_in_clusters_final]["Merged"]:
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'] = []
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'].extend(gl_dev['SSID'])
						else:
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'] = dt.union_of_lists_wo_repetitions([
																											clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'],
																											gl_dev['SSID']
																										])
					# append device to list of seperete devices
					clusters_final[index_of_random_device_in_clusters_final]["Seperated"].append(gl_dev) # append them from behind beacuse first ones may are already be occupied by different OUI(s)

					total_devices = num_of_clustered_dev+num_of_unclustered_dev # global device is merged to one of random devices

				else: # if mins[0][0] < DISTANCE_THRESHOLD:
					# no matching found; clusters final is same as clusters after first stage and global device is added to unclustered devices
					clusters_final[0].append(global_devices[0])
					total_devices = num_of_clustered_dev + num_of_unclustered_dev + num_of_global_dev


			# 1 random and >=2 global devices
			elif ( ((num_of_clustered_dev+num_of_unclustered_dev)==1) and (num_of_global_dev>1) ):
				print("# 1 random and >=2 global devices")
				# "matching radnom to global devices"
				mins = Min_2(X[num_of_global_dev][0:num_of_global_dev]) # find two minimums of first row in X
				
				if ( (mins[0][0]<DISTANCE_THRESHOLD) and (mins[0][0]*KOEFF_DIFF<mins[1][0]) ):
					# group two devices together
					print("Matched")
					gl_dev = global_devices[mins[0][1]]

					if num_of_unclustered_dev > 0 : # if random device is unclustered one
						random_dev = clusters_final[0].pop(0) # remove random device from unclustered list since it is now clustered
						clusters_final.append({"Seperated":[]})

						clusters_final[-1]["Merged"] = copy.deepcopy(random_dev) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
						clusters_final[-1]["Seperated"].append(random_dev) # append one above for saving all probe requests to and this one for archiving all different probe requests from different MACs
						clusters_final[-1]["Merged"]["MAC"] = [ random_dev["MAC"] ] # make array where MACs will be stored and save MAC address of first MAC

						index_of_random_device_in_clusters_final = -1

					else: # num_of_clustered_dev > 0 -> random device is clustered one
						index_of_random_device_in_clusters_final = -1 # same as above; this line could be removed but due to understanding the code I kept it

					#append global device to random (check for repeating probe requests)
					different_probe_req_buffer = []
					for pr_reqs_i in gl_dev['PROBE_REQs']:
						found_same_probe_req = False
						for pr_reqs_0 in clusters_final[index_of_random_device_in_clusters_final]["Merged"]['PROBE_REQs']:
							if pr_reqs_i["DATA"]==pr_reqs_0["DATA"]:
								pr_reqs_0["TIME"].extend(pr_reqs_i["TIME"])
								pr_reqs_0["RSSI"].extend(pr_reqs_i["RSSI"])
								found_same_probe_req = True
								break
						if found_same_probe_req == False:
							different_probe_req_buffer.append(pr_reqs_i)
					
					clusters_final[index_of_random_device_in_clusters_final]["Merged"]['PROBE_REQs'].extend(different_probe_req_buffer)

					# append MAC address to MAC addresses list
					clusters_final[index_of_random_device_in_clusters_final]["Merged"]["MAC"].append(gl_dev["MAC"])

					# check if global device even has SSID key (PROBE_REQs key is there by default even it is empyt)
					if 'SSID' in gl_dev:
						if 'SSID' not in clusters_final[index_of_random_device_in_clusters_final]["Merged"]:
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'] = []
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'].extend(gl_dev['SSID'])
						else:
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'] = dt.union_of_lists_wo_repetitions([
																											clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'],
																											gl_dev['SSID']
																										])
					# append device to list of seperete devices
					clusters_final[index_of_random_device_in_clusters_final]["Seperated"].append(gl_dev) # append them from behind beacuse first ones may are already be occupied by different OUI(s)

					total_devices = num_of_global_dev # only one random device is merged to one of global devices
					
				else: # if mins[0][0] < DISTANCE_THRESHOLD:
					# no matching found; clusters final is same as clusters after first stage and all global devices are extended to unclustered devices
					clusters_final[0].extend(global_devices)
					total_devices = num_of_clustered_dev + num_of_unclustered_dev + num_of_global_dev


			# 1 random and 1 global devices
			elif ( ((num_of_clustered_dev+num_of_unclustered_dev)==1) and (num_of_global_dev==1) ):
				print("# 1 random and 1 global devices")
				if ( X[0][1]<DISTANCE_THRESHOLD  ):
					print("Matched")
					gl_dev = global_devices[0]

					if num_of_unclustered_dev > 0 : # if random device is unclustered one
						random_dev = clusters_final[0].pop(0) # remove random device from unclustered list since it is now clustered
						clusters_final.append({"Seperated":[]})

						clusters_final[-1]["Merged"] = copy.deepcopy(random_dev) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
						clusters_final[-1]["Seperated"].append(random_dev) # append one above for saving all probe requests to and this one for archiving all different probe requests from different MACs
						clusters_final[-1]["Merged"]["MAC"] = [ random_dev["MAC"] ] # make array where MACs will be stored and save MAC address of first MAC

						index_of_random_device_in_clusters_final = -1

					else: # num_of_clustered_dev > 0 -> random device is clustered one
						index_of_random_device_in_clusters_final = -1 # same as above; this line could be removed but due to understanding the code I kept it

					#append global device to random (check for repeating probe requests)
					different_probe_req_buffer = []
					for pr_reqs_i in gl_dev['PROBE_REQs']:
						found_same_probe_req = False
						for pr_reqs_0 in clusters_final[index_of_random_device_in_clusters_final]["Merged"]['PROBE_REQs']:
							if pr_reqs_i["DATA"]==pr_reqs_0["DATA"]:
								pr_reqs_0["TIME"].extend(pr_reqs_i["TIME"])
								pr_reqs_0["RSSI"].extend(pr_reqs_i["RSSI"])
								found_same_probe_req = True
								break
						if found_same_probe_req == False:
							different_probe_req_buffer.append(pr_reqs_i)
					
					clusters_final[index_of_random_device_in_clusters_final]["Merged"]['PROBE_REQs'].extend(different_probe_req_buffer)

					# append MAC address to MAC addresses list
					clusters_final[index_of_random_device_in_clusters_final]["Merged"]["MAC"].append(gl_dev["MAC"])

					# check if global device even has SSID key (PROBE_REQs key is there by default even it is empyt)
					if 'SSID' in gl_dev:
						if 'SSID' not in clusters_final[index_of_random_device_in_clusters_final]["Merged"]:
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'] = []
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'].extend(gl_dev['SSID'])
						else:
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'] = dt.union_of_lists_wo_repetitions([
																											clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'],
																											gl_dev['SSID']
																										])
					# append device to list of seperete devices
					clusters_final[index_of_random_device_in_clusters_final]["Seperated"].append(gl_dev) # append them from behind beacuse first ones may are already be occupied by different OUI(s)

					total_devices = num_of_global_dev # only one random device is merged to one of global devices
					
				else: # if ( X[0][1] < DISTANCE_THRESHOLD  ):
					# no matching found; clusters final is same as clusters after first stage and all global devices are extended to unclustered devices
					clusters_final[0].extend(global_devices)
					total_devices = num_of_clustered_dev + num_of_unclustered_dev + num_of_global_dev


				














			# >=2 random and >=2 global devices
			else:
				print("# >=2 random and >=2 global devices")
				num_of_matched = 0
				# loop throuh all global devices (rows) in X
				for i,gl_dev_dists in enumerate(X[0:num_of_global_dev]):
					gl_device = global_devices[i]
					
					mins = Min_2(gl_dev_dists[num_of_global_dev:])
					matched = False
					
					if ( (mins[0][0] < DISTANCE_THRESHOLD) and (mins[0][0]*KOEFF_DIFF<mins[1][0]) ):

						mins_2 = Min_2(X[num_of_global_dev+mins[0][1]][0:num_of_global_dev])

						if ( (mins_2[0][0]==mins[0][0]) and (mins_2[0][0]*KOEFF_DIFF<mins_2[1][0]) ):
							print("Matched")
							matched = True
							num_of_matched += 1

							if mins[0][1] < num_of_unclustered_dev: # if index of random devices in under unclustered devices -> pop it from unclustered and add new cluster with these two devices
								random_dev = clusters_final[0].pop(mins[0][1]) # remove random device from unclustered list since it is now clustered
								clusters_final.append({"Seperated":[]})
								clusters_final[-1]["Merged"] = copy.deepcopy(random_dev) # append them from behind beacuse first ones may are already be occupied by different OUI(s)
								clusters_final[-1]["Seperated"].append(random_dev) # append one above for saving all probe requests to and this one for archiving all different probe requests from different MACs
								clusters_final[-1]["Merged"]["MAC"] = [ random_dev["MAC"] ] # make array where MACs will be stored and save MAC address of first MAC

								index_of_random_device_in_clusters_final = -1					

							else: # index of random devices is under clustered devices
								index_of_random_device_in_clusters_final = mins[0][1]-num_of_unclustered_dev+1

							#append global device to random (check for repeating probe requests)
							different_probe_req_buffer = []
							for pr_reqs_i in gl_device['PROBE_REQs']:
								found_same_probe_req = False
								for pr_reqs_0 in clusters_final[index_of_random_device_in_clusters_final]["Merged"]['PROBE_REQs']:
									if pr_reqs_i["DATA"]==pr_reqs_0["DATA"]:
										pr_reqs_0["TIME"].extend(pr_reqs_i["TIME"])
										pr_reqs_0["RSSI"].extend(pr_reqs_i["RSSI"])
										found_same_probe_req = True
										break
								if found_same_probe_req == False:
									different_probe_req_buffer.append(pr_reqs_i)
							
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]['PROBE_REQs'].extend(different_probe_req_buffer)

							# append MAC address to MAC addresses list
							clusters_final[index_of_random_device_in_clusters_final]["Merged"]["MAC"].append(gl_device["MAC"])

							# check if global device even has SSID key (PROBE_REQs key is there by default even it is empyt)
							if 'SSID' in gl_device:
								if 'SSID' not in clusters_final[index_of_random_device_in_clusters_final]["Merged"]:
									clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'] = []
									clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'].extend(gl_device['SSID'])
								else:
									clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'] = dt.union_of_lists_wo_repetitions([
																													clusters_final[index_of_random_device_in_clusters_final]["Merged"]['SSID'],
																													gl_device['SSID']
																												])
							# append device to list of seperete devices
							clusters_final[index_of_random_device_in_clusters_final]["Seperated"].append(gl_device) # append them from behind beacuse first ones may are already be occupied by different OUI(s)


					
					if not matched:
						clusters_final[0].append(gl_device)

				total_devices = num_of_clustered_dev + num_of_unclustered_dev + num_of_global_dev - num_of_matched




	elif no_random_devs == True:
		# no random devices, just global >= 1
		total_devices = num_of_global_dev
		clusters_final[0] = global_devices 
	
	elif num_of_global_dev==0:
		# no global devices and just random devices >=1
		total_devices = num_of_clustered_dev+num_of_unclustered_dev
		clusters_final = clusters


else: #if len(probe_reqs) > 0:
	total_devices = 0




# #######################################         STEP 4         #######################################
# # Count number of generated clusters (number of devices)
print(f'Global devices start: {len(global_devices)}')

#count random devices
count = 0
for rnd_cl in random_devices:
	count += len(random_devices[rnd_cl])
print(f'Random devices before: {count}')
print(f'Total_devices identified: {total_devices}') 




 
    







