import requests
import sys
import os
from datetime import datetime
from requests_futures.sessions import FuturesSession
from concurrent.futures import as_completed, wait
import math

_async_requests = []
_MAX_POINTS = 10000

def get_basepath():
    if 'true' == os.getenv('CONTAINER', 'false'):
        return 'http://api:3000/api/v1/datapoints/'
    elif 'true' == os.getenv('DEVELOPMENT', 'false'):
        return 'http://localhost/api/v1/datapoints/'
    else:
        return 'https://e6-dataproc.ijs.si/api/v1/datapoints/'


_params = None
def get_parameters():
    """Parse command line arguments and extract parameters or use ones that were set"""
    if not _params is None:
        return _params
    
    args = sys.argv[1]
    dot_sepparated = args.split('.')
    file_ending = dot_sepparated.pop()
    
    just_params = '.'.join(dot_sepparated)
    splitted  = just_params.split('%')
    return {'fileEnd': file_ending,
            'name': splitted[0],
            'dataStreamId': splitted[1].split(','),
            'begin': splitted[2],
            'end': splitted[3],
            'extra': splitted[4:]}


def get_number(stream_index = 0):
    """Returns number of datapoints that should be fetched"""
    params = get_parameters()
    path = f"{get_basepath()}count/{params['dataStreamId'][stream_index]}"
    response = requests.get(path, params={'begin':params['begin'], 'end':params['end']})
    response.raise_for_status()
    return response.json()["value"]


def get_filename():
    path = ''
    if 'true' == os.getenv('CONTAINER', 'false'):
        path = '/usr/src/app/web/visualizations/'
        
    return path + sys.argv[1]


def get_intervals(num_of_intervals):
    """Returns list of time intervals for data fetch. List of length num_of_intervals"""
    
    def correct_tz(time_str):
        """Adds '+' to timezone part of datetime"""
        splitted = time_str.split('.')
        tz = splitted[1]

        if len(tz) == 4:
            tz = '0' + tz
        
        if not tz[0] in ['+', '-']:
            tz = '+' + tz
        return '.'.join([splitted[0], tz])
        
    params = get_parameters()

    begin = datetime.strptime(correct_tz(params['begin']), '%Y-%m-%dT%H:%M:%S.%zZ')
    end = datetime.strptime(correct_tz(params['end']), '%Y-%m-%dT%H:%M:%S.%zZ')

    num_of_intervals = math.ceil(num_of_intervals / 60.) * 60

    if num_of_intervals == 0:
        return [(begin, end)]
    
    step = (end - begin) / num_of_intervals
    #print(step)
    
    intervals = []
    while begin < end:
        if begin + step <= end:
            intervals.append((begin, begin + step))
        else:
            intervals.append((begin, end))
        prev_begin = begin
        begin += step

    #print(intervals)
        
    return intervals


def set_params(dataStreamId, begin, end):
    """Set parameters instead of reading them from a filename
    args: - dataStreamId: list of integers
          - begin: ISO 8061 time string
          - end: ISO 8061 time string
    """
    global _params
    _params = {'begin': begin,
               'end': end,
               'dataStreamId': dataStreamId}
    
    

def get_datapoints(stream_index = 0, chunk_size = _MAX_POINTS):
    """Download data points in chunks and return one list of data points.
    If multiple data streams are requested then get stream_index one.
    """
    global _async_requests
    
    points_count = get_number(stream_index)
    interval_num = math.floor(points_count / chunk_size)
    intervals = get_intervals(interval_num)
    params = get_parameters()
    path = get_basepath() + str(params['dataStreamId'][stream_index])

    datapoints = []
    print(f"Download divided in {len(intervals)} intervals")
    
    with FuturesSession() as session:
        # send multiple requests
        for begin, end in intervals:
            req_future = session.get(path, params={'begin': begin, 'end': end})
            _async_requests.append(req_future)

        # wait for them to finish
        wait(_async_requests)

        # merge results together                                   
        for future in _async_requests:                             
            response = future.result()                           
            if response.status_code == 404:                         
                # empty interval returns 404 so we can safely ignore
                continue                                           
            response.raise_for_status()                             
            datapoints.extend(response.json())      

        _async_requests = []

    return datapoints
