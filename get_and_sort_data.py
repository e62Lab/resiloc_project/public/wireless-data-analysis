import ijs_measurements.client as ijs
from datetime import datetime
import argparse
from datetime import datetime , timedelta ,timezone
import requests

DOWNLOAD_DATA_IN_MULTIPLE_CHUNCKS = True
DATASTREAM = [25]
FILENAME = "sorted_data_new_get.json"

current_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.000Z")

print(f"Current time: {current_time}")


# parser test:
# no argumnets:python3 get_and_sort_data.py 
# mode = 'rel':
	# just hours: python3 get_and_sort_data.py --mode "rel" --h 2
	# just minutes: python3 get_and_sort_data.py --mode "rel" --m 2
	# hours and minutes: python3 get_and_sort_data.py --mode "rel" --m 2 --h 2
	# with -l (no effect predicted): python3 get_and_sort_data.py --mode "rel" -l --m 2 --h 2
# mode = 'abs':
	#  no startTime and stopTime provided (not valid): python3 get_and_sort_data.py --mode "abs"
	# with startTime: python3 get_and_sort_data.py --mode "abs" --startTime "2021-12-13T15:18:51"
	# with startTime and local: python3 get_and_sort_data.py --mode "abs" --startTime "2021-12-13T15:18:51" -l
	# with startTime and local and hours and minutes:  python3 get_and_sort_data.py --mode "abs" --startTime "2021-12-13T15:18:51" -l --h 2 --m 2
	# with both times: python3 get_and_sort_data.py --mode "abs" --startTime "2021-12-13T15:18:51" --stopTime "2021-12-13T17:18:51" -l
	# startTime > stopTime (not valied): python3 get_and_sort_data.py --mode "abs" --startTime "2021-12-13T15:18:51" --stopTime "2021-12-13T11:18:51" -l
# mode = "krneki" (not a valid option)
	# python3 get_and_sort_data.py --mode "krneki"

# this block handles system arguments
parser = argparse.ArgumentParser(allow_abbrev=False)
parser.add_argument('--startTime', type=str, default= None, help="ISO 8601 time string that data is taken from database")
parser.add_argument('--stopTime', type=str, default= None, help="ISO 8601 time string that data is taken from database")
# "%Y-%m-%dT%H:%M:%S"
parser.add_argument('-l', default= None, action="store_true", help="In local time")
parser.add_argument('--mode', dest='mode', type=str, choices=['rel', 'abs'], default= None, help="Specify in which of two modes you will specify time that corresponed to data being taken from database. Two modes are rel and abs . rel: take data relative to current time back.If you use this switch you shoul also use at least one of the following switches: -h, -m. abs: specify string dates for abosulute time interval or one string (the other one is current time) or one string and addidtional switches for -h and -m to specify relative time back.")
parser.add_argument('--h', type=int, default=None, help="It specifys number of hours from current time data is taken from database")
parser.add_argument('--m', type=int, default=None, help="It specifys number of minutes from current time data is taken from database")

#start_time < stop_time

args = parser.parse_args()

# print(args.startTime)
# print(args.stopTime)
# print(args.l)
# print(args.mode)
# print(args.h)
# print(args.m)


start_time = None
stop_time = None

if args.mode != None:
	if args.mode == "rel":
		if args.h != None:
			hours = args.h
		else:
			hours = 0
		if args.m != None:
			minutes = args.m
		else:
			minutes = 0
		
		if args.h != None or args.m != None:
			stop_time = datetime.now(timezone.utc)
			# stop_time_string = stop_time.strftime("%Y-%m-%dT%H:%M:%S.000Z")
			delta_time = timedelta(hours=hours, minutes=minutes)		
			start_time = stop_time-delta_time
		else:
			print("At least on argumnet of -h or -m must be provided.")


	elif args.mode == "abs":

		if args.stopTime==None and args.startTime!=None:
			if args.l:
				# convert to UTC time
				start_time = datetime.strptime(args.startTime, "%Y-%m-%dT%H:%M:%S") # get local time
				zone_delta_time = start_time.astimezone().tzinfo.utcoffset(None)
				start_time = start_time-zone_delta_time
				
			else:	
				start_time = datetime.strptime(args.startTime, "%Y-%m-%dT%H:%M:%S") # get UTC time

			if args.h==None and args.m==None:
				stop_time = datetime.now(timezone.utc)
			else:
				if args.h != None:
					hours = args.h
				else:
					hours = 0
				if args.m != None:
					minutes = args.m
				else:
					minutes = 0
				delta_time = timedelta(hours=hours, minutes=minutes)
				stop_time = start_time+delta_time

		elif args.stopTime!=None and args.startTime!=None:
			if args.l:
				# convert to UTC time
				stop_time = datetime.strptime(args.stopTime, "%Y-%m-%dT%H:%M:%S") # get local time
				zone_delta_time = stop_time.astimezone().tzinfo.utcoffset(None)
				stop_time = stop_time-zone_delta_time

				start_time = datetime.strptime(args.startTime, "%Y-%m-%dT%H:%M:%S") # get local time
				zone_delta_time = start_time.astimezone().tzinfo.utcoffset(None)
				start_time = start_time-zone_delta_time

			else:
				stop_time = datetime.strptime(args.stopTime, "%Y-%m-%dT%H:%M:%S") # get UTC time
				start_time = datetime.strptime(args.startTime, "%Y-%m-%dT%H:%M:%S") # get UTC time
			if (start_time > stop_time):
				raise "Start time is greater than stop time"

		else:
			raise "Specify at least --startTime for --mode = 'abs'"

	else:
		pass
		# raise "Not correct argument for switch --mode" # it will not come to this beacuse this case is already handled by argument parser
    
else:
	stop_time = datetime.now(timezone.utc)
	# stop_time_string = stop_time.strftime("%Y-%m-%dT%H:%M:%S.000Z")
	delta_time = timedelta(hours=1)
	start_time = stop_time-delta_time


stop_time_string = stop_time.strftime("%Y-%m-%dT%H:%M:%S.000Z")
start_time_string = start_time.strftime("%Y-%m-%dT%H:%M:%S.000Z")

print(f'Stop time: {stop_time_string}')
print(f'Start time: {start_time_string}')



# # ijs.set_params([25], "2021-09-17T15:22:55.000Z", "2021-09-17T15:23:07.000Z")
# ijs.set_params([25], "2021-09-17T15:22:55.000Z", current_time)
# # ijs.set_params([25], "2021-10-17T12:46:08.223Z", current_time)
# # ijs.set_params([25], "2021-10-18T13:04:23.428Z", "2021-11-18T15:15:55.000Z")

ijs.set_params(DATASTREAM, start_time_string, stop_time_string)


if 	DOWNLOAD_DATA_IN_MULTIPLE_CHUNCKS:
    data = ijs.get_datapoints()
else:
    def correct_tz(time_str):
        """Adds '+' to timezone part of datetime"""
        splitted = time_str.split('.')
        tz = splitted[1]

        if len(tz) == 4:
            tz = '0' + tz
        
        if not tz[0] in ['+', '-']:
            tz = '+' + tz
        return '.'.join([splitted[0], tz])

    params = ijs.get_parameters()    
    begin = datetime.strptime(correct_tz(params['begin']), '%Y-%m-%dT%H:%M:%S.%zZ')
    end = datetime.strptime(correct_tz(params['end']), '%Y-%m-%dT%H:%M:%S.%zZ')
    print(begin)
    print (end)
    path = ijs.get_basepath() + str(params['dataStreamId'][0])
    response = requests.get(path, params={'begin': begin, 'end': end})
    print(response.url)
    response.raise_for_status()
    # print(f"Resonse: {response.text}")
    print(f"Request was made to: {response.url}")
    print(f"Response code: {response.status_code}")
    data = response.json()
    # print(data)


dates = []
for i, point in enumerate(data):
    #scans = point["payload"]["scanned_device"]
    #diff(scans, prev)
    #prev = scans
    
    date_str = point["timestamp"][:19]
    date = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S")
            
    data[i]["timestamp"] = date

import json
with open(FILENAME, "w") as out:
    out.write(json.dumps([ {"dataStreamId":i["dataStreamId"], "payload":i["payload"], "timestamp":str(i["timestamp"])} for i in sorted(data, key=lambda x: x["timestamp"]) ] ) )
