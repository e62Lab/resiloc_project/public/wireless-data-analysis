import pyshark
import os
import json


MERGE_DATA = True
MERGE_ALL = True
MERGE = ['A', 'E', 'I', 'J', 'K', 'L','M', 'N', 'O', 'R', 'S', 'T', 'U', 'V', 'X']
# MERGE = ['R', 'K', 'U', 'V', 'X']
MERGE = ['M', 'U']
MERGE = ['V', 'L', 'E']
MERGE = ['T', 'O']
MERGE = ['M', 'U', 'K']
MERGE = ['E', 'J']
MERGE = ['H', 'I']
MERGE = ['N', 'X']
MERGE = ['B', 'J']



DEVICE_LABEL_KEY = "LABEL"

TAG_NUM_SSID = "0"
TAG_NUM_SUPP_DATA_RTS = "1"
#TAG_NUM_HT_CAP = "26"
TAG_NUM_HT_CAP = "45"
TAG_NUM_EXT_SUPP_DATA_RTS = "50"
TAG_NUM_INTERWORKING = "107"
TAG_NUM_EXT_CAP = "127"
TAG_NUM_VHT_CAP = "191"
TAG_NUM_VENDOR = "221"
RSSI_KEY = "RSSI"
TIME_KEY = "TIME"
SSID_KEY = "SSID"
DATA_RTS_KEY = "DATA_RTS"
SUPP_DATA_RTS_KEY = "SUPP"
EXT_SUPP_DATA_RTS_KEY = "EXT"
HT_CAP_KEY = "HT_CAP"
EXT_CAP_KEY = "EXT_CAP"
VHT_CAP_KEY = "VHT_CAP"
INTERWORKING_KEY = "INTWORK"
VENDOR_SPEC_KEY = "VENDOR_SPEC"
EXT_TAG_KEY = "EXT_TAG"
PROBE_REQs_KEY = "PROBE_REQs"




def deviceScannedParsedData(mac, ssid, sniff_time, rssi, pr_data, all_devices, device_label_val) -> None:

                # self._parentComm.put(
                #     {
                #         "type": "wifi_parsed_data",
                #         "scanned_device": self.all_devices,
                #         "start_timestamp": round(self._first_timestamp_parsed),
                #         "end_timestamp": round(self._first_timestamp_parsed)
                #         + self._time_scan,
                #     }
                # )


        # pr_N={
        #       'TIME': [X],
        #       'RSSI': [X],
        #       'DATA':{
        #           'SUPP_DATA_RTS': X,
        #           'EXT_DATA_RTS': X,
        #           'HT_CAP': X,
        #           'EXT_CAP': {'length':X, 'data':Y},
        #           'VHT_CAP': X,
        #           'INTERWORKING': X,
        #           'EXT_TAG': {'ID_1': DATA_1, 'ID_2': DATA_2,..},
        #           'VENDOR_SPEC': {VENDOR_1:{'ID_1': DATA_1, 'ID_2': DATA_2,..}, VENDOR_2:{'ID_1': DATA_1, 'ID_2': DATA_2,..}} } }
        #
        # {'MAC': mac_1, 'SSIDs': [SSID_1, SSID_2,..], 'PROBE_REQs': [pr_1, pr_2,...] }

    # TODO: if you loop in reverse order than loop will break quicker since it is more probbable that new mac is the same as last one (burst nature of probe requests)
    new_device = True
    for device in all_devices:
        if mac == device["MAC"]:
            # add SSID to list of known SSID for particular MAC address
            if len(ssid) > 0:
                # check if SSID key even exists (no SSID entry before)
                if SSID_KEY not in device:
                    device[SSID_KEY] = []
                if ssid not in device[SSID_KEY]:
                    device[SSID_KEY].append(ssid)

            update_prs = True
            for pr_old in device[PROBE_REQs_KEY]:
                if pr_old["DATA"] == pr_data:
                    pr_old[TIME_KEY].append(
                        sniff_time
                    )  # will update all_devices
                    pr_old[RSSI_KEY].append(
                        rssi
                    )  # will update all_devices
                    update_prs = False
                    break

            if update_prs:
                new_pr = {TIME_KEY: [sniff_time], RSSI_KEY: [rssi], "DATA": pr_data}
                device[PROBE_REQs_KEY].append(new_pr)
            new_device = False
            break
    if new_device:
        new_pr = {TIME_KEY: [sniff_time], RSSI_KEY: [rssi], "DATA": pr_data}
        new_dev = {"MAC": mac, DEVICE_LABEL_KEY: device_label_val, PROBE_REQs_KEY: [new_pr]}
        if len(ssid) > 0:
            new_dev[SSID_KEY] = [ssid]
        all_devices.append(new_dev)









different_tags_order = {}






if MERGE_DATA:
    devices = list()


datasets_parsed_dir = "./dataset/datasets_parsed/"
devices_folders_dir = "./dataset/A dataset of labelled device Wi-Fi probe requests for MAC address de-randomization - 2021/"
device_folders = os.listdir(devices_folders_dir)
for device_folder in device_folders:
    different_tags_order[device_folder] = []
    
    
    
    print(device_folder)
    if (( (not MERGE_DATA) or (MERGE_DATA and MERGE_ALL)) and "data_dev" in device_folder) or ( (MERGE_DATA and not MERGE_ALL) and device_folder[8] in MERGE ):

        if not MERGE_DATA:
            devices = list()

        pcap_files = os.listdir(f'{devices_folders_dir}{device_folder}')
        # only get probe requests
        bpf_filt = "subtype probe-req"

        for pcap_file in pcap_files:
            capture = pyshark.FileCapture(f'{devices_folders_dir}{device_folder}/{pcap_file}',
                                        use_json=True,
                                        include_raw=True,
                                        )
            # print(capture)


            for raw_packet in capture:

                tag_numbers = []
                for i in range(0,len(raw_packet[7].all.tag)):
                    tag_numbers.append(raw_packet[7].all.tag[i].number)

                # print(tag_numers)

                if tag_numbers not in different_tags_order[device_folder]:
                    # different_tags_order.append(tag_numbers)
                    different_tags_order[device_folder].append(tag_numbers)







                # Get relevant wlan data
                pr_data = {}
                # save MAC address
                mac = raw_packet.wlan.sa

                # save time when packet was captured
                sniff_time = raw_packet.sniff_timestamp
                # save RSSI
                rssi = raw_packet.wlan_radio.signal_dbm

                # loop through all tags that are present in packet
                for i, tag in enumerate(
                    raw_packet[7]._all_fields["wlan.tagged.all"]["wlan.tag"]
                ):
                    tag_num = tag["wlan.tag.number"]
                    if tag_num == TAG_NUM_SSID:
                        # 'wlan.ssid': ''
                        ssid = tag["wlan.ssid"]

                    elif tag_num == TAG_NUM_SUPP_DATA_RTS:
                        # 'wlan.supported_rates': ['130', '132', '139', '150', '12', '18', '24', '36']
                        supp_data_rts = tag["wlan.supported_rates"]
                        if not (DATA_RTS_KEY in pr_data):
                            pr_data[DATA_RTS_KEY] = {}
                        pr_data[DATA_RTS_KEY][SUPP_DATA_RTS_KEY] = supp_data_rts

                    elif tag_num == TAG_NUM_HT_CAP:
                        # 2B: 'wlan.ht.capabilities':'0x000000ef'
                        # 1B: 'wlan.ht.ampduparam':'0x00000013'
                        # : 'wlan.ht.mcsset.rxbitmask':{'wlan.ht.mcsset.rxbitmask.0to7': '0x000000ff', 'wlan.ht.mcsset.rxbitmask.8to15': '0x000000ff', 'wlan.ht.mcsset.rxbit...ask.16to23': '0x00000000', 'wlan.ht.mcsset.rxbit...ask.24to31': '0x00000000', 'wlan.ht.mcsset.rxbitmask.32': '0x00000000', 'wlan.ht.mcsset.rxbit...ask.33to38': '0x00000000', 'wlan.ht.mcsset.rxbit...ask.39to52': '0x00000000', 'wlan.ht.mcsset.rxbit...ask.53to76': '0x00000000'}
                        # 10b: 'wlan.ht.mcsset.highestdatarate':'0x00000000'
                        # 1b: 'wlan.ht.mcsset.txsetdefined':'0'
                        # 1b: 'wlan.ht.mcsset.txrxmcsnotequal':'0'
                        # 2b: 'wlan.ht.mcsset.txmaxss':'0x00000000'
                        # 1b: 'wlan.ht.mcsset.txunequalmod':'0'
                        # ----------------------------------------
                        # 16B: mcsset
                        # 2B: 'wlan.htex.capabilities':'0x00000000'
                        # 4B: 'wlan.txbf':'0x00000000'
                        # 1B: 'wlan.asel':'0x00000000'
                        # ----------------------------------------
                        # ----------------------------------------
                        # = 26B
                        ht_cap_raw_hex = raw_packet[7]._all_fields["wlan.tagged.all"][
                            "wlan.tag_raw"
                        ][i][0]
                        ht_cap_raw_hex = (
                            "0x" + ht_cap_raw_hex[4:]
                        )  # remove tag ID and tag length from raw bits and append 0x so it is know that it is a hex num
                        pr_data[HT_CAP_KEY] = ht_cap_raw_hex
                    elif tag_num == TAG_NUM_EXT_SUPP_DATA_RTS:
                        # 'wlan.extended_supported_rates': ['12', '18', '24', '36', '48', '72', '96', '108']
                        ext_supp_data_rts = tag["wlan.extended_supported_rates"]
                        if not (DATA_RTS_KEY in pr_data):
                            pr_data[DATA_RTS_KEY] = {}
                        pr_data[DATA_RTS_KEY][EXT_SUPP_DATA_RTS_KEY] = ext_supp_data_rts

                    elif tag_num == TAG_NUM_INTERWORKING:
                        # 1-7B usually long (variable length)
                        interwoking_raw_hex = raw_packet[7]._all_fields[
                            "wlan.tagged.all"
                        ]["wlan.tag_raw"][i][0]
                        interwoking_raw_hex = (
                            "0x" + interwoking_raw_hex[4:]
                        )  # remove tag ID and tag length from raw bits and append 0x so it is know that it is a hex num
                        pr_data[INTERWORKING_KEY] = interwoking_raw_hex

                    elif tag_num == TAG_NUM_EXT_CAP:
                        # 8* 1B: 'wlan.extcap': ['0x00000004', '0x00000000', '0x00000002', '0x00000082', '0x00000000', '0x00000040', '0x00000000', '0x00008040', '0x00000001']
                        # ext_cap_stripped = ['0x'+hexa[-2:] for hexa in tag['wlan.extcap']] # ['0x04', '0x00', '0x02', '0x82', '0x00', '0x40', '0x00', '0x40', '0x01']
                        # ext_cap = merge_hex_nums(ext_cap_stripped)
                        # length of EXT_CAP is variable!
                        # length_in_bytes = tag['wlan.tag.length']
                        ext_cap_raw_hex = raw_packet[7]._all_fields["wlan.tagged.all"][
                            "wlan.tag_raw"
                        ][i][0]
                        ext_cap_raw_hex = (
                            "0x" + ext_cap_raw_hex[4:]
                        )  # remove tag ID and tag length from raw bits and append 0x so it is know that it is a hex num
                        pr_data[EXT_CAP_KEY] = ext_cap_raw_hex
                    elif tag_num == TAG_NUM_VHT_CAP:
                        # 4B: 'wlan.vht.capabilities':'0x3390f092' No stripping needed
                        # 'wlan.vht.mcsset.rxmcsmap':'0x0000fffa'
                        # 'wlan.vht.mcsset.rxhighestlonggirate':'0x00000362'
                        # 'wlan.vht.mcsset.max_nsts_total':'0'
                        # 'wlan.vht.mcsset.txmcsmap':'0x0000fffa'
                        # 'wlan.vht.mcsset.txhighestlonggirate':'0x00000362'
                        # 'wlan.vht.ncsset.ext_nss_bw_cap':'0'
                        # 'wlan.vht.ncsset.reserved':'0x00000000'
                        # -------------------------------------------
                        # 8B

                        # -------------------------------------------
                        # -------------------------------------------
                        # = 12B
                        vht_cap_raw_hex = raw_packet[7]._all_fields["wlan.tagged.all"][
                            "wlan.tag_raw"
                        ][i][0]
                        vht_cap_raw_hex = (
                            "0x" + vht_cap_raw_hex[4:]
                        )  # remove tag ID and tag length from raw bits and append 0x so it is know that it is a hex num
                        pr_data[VHT_CAP_KEY] = vht_cap_raw_hex

                    elif tag_num == TAG_NUM_VENDOR:
                        oui = tag["wlan.tag.oui_raw"][0]  # vendor
                        length = int(tag["wlan.tag.length"])
                        data_id = tag["wlan.tag.vendor.oui.type"]
                        data = (
                            "0x"
                            + raw_packet[7]._all_fields["wlan.tagged.all"][
                                "wlan.tag_raw"
                            ][i][0][-(length - 4) * 2 :]
                        )
                        # check if vendor already exists
                        if VENDOR_SPEC_KEY not in pr_data:
                            pr_data[VENDOR_SPEC_KEY] = dict()
                        if oui in pr_data[VENDOR_SPEC_KEY].keys():
                            pr_data[VENDOR_SPEC_KEY][oui][data_id] = data
                        else:
                            pr_data[VENDOR_SPEC_KEY][oui] = {}
                            pr_data[VENDOR_SPEC_KEY][oui][data_id] = data

                # check if extended tags are present
                if "wlan.ext_tag" in raw_packet[7]._all_fields["wlan.tagged.all"]:
                    # if ext_tag contain more than one elements then they are stored in a list
                    if (
                        type(
                            raw_packet[7]._all_fields["wlan.tagged.all"]["wlan.ext_tag"]
                        )
                        == list
                    ):
                        pr_data[EXT_TAG_KEY] = {}
                        for i, ext_tag in enumerate(
                            raw_packet[7]._all_fields["wlan.tagged.all"]["wlan.ext_tag"]
                        ):
                            tag_num = ext_tag["wlan.ext_tag.number"]
                            # length_data_in_bytes = ext_tag['wlan.ext_tag.length']
                            data_raw_hex = raw_packet[7]._all_fields["wlan.tagged.all"][
                                "wlan.ext_tag_raw"
                            ][i][0]
                            data_raw_hex = (
                                "0x" + data_raw_hex[2:]
                            )  # remove ext_tag ID from raw bits and append 0x so it is know that it is a hex num
                            pr_data[EXT_TAG_KEY][tag_num] = data_raw_hex
                    # if ext_tag contain just one element
                    else:
                        ext_tag = raw_packet[7]._all_fields["wlan.tagged.all"][
                            "wlan.ext_tag"
                        ]
                        tag_num = ext_tag["wlan.ext_tag.number"]
                        # length_data_in_bytes = ext_tag['wlan.ext_tag.length']
                        data_raw_hex = raw_packet[7]._all_fields["wlan.tagged.all"][
                            "wlan.ext_tag_raw"
                        ][0]
                        data_raw_hex = (
                            "0x" + data_raw_hex[2:]
                        )  # remove ext_tag ID from raw bits and append 0x so it is know that it is a hex num
                        pr_data[EXT_TAG_KEY] = {}
                        pr_data[EXT_TAG_KEY][tag_num] = data_raw_hex


                # print(f'SSID: {ssid}')
                # print(f'mac: {mac}')
                # print(f'sniff_time: {sniff_time}')
                # print(f'RSSI: {rssi}')
                # print(f'PR: {pr_data}')
                deviceScannedParsedData(mac, ssid, sniff_time, rssi, pr_data, devices, device_folder[8])


        # device = device_folder[8]
        if not MERGE_DATA:
            file_to_write = open(f'{datasets_parsed_dir}{device_folder}', 'w')
            json_devices = json.dumps(devices)
            file_to_write.write(json_devices)
            file_to_write.close()
            print(devices)

    # print(10*'\n')
    # print(different_tags_order)
    # different_tags_order =  []
    # print(10*'\n')
    # different_tags_order.append(device_folder)
    

if MERGE_DATA:
    # print(devices)
    if MERGE_ALL:
        
        
        file_to_write = open(f'{datasets_parsed_dir}data_devALL_dataset', 'w')
    else:
        file_to_write = open(f'{datasets_parsed_dir}data_dev{"".join(MERGE)}_dataset', 'w')
    json_devices = json.dumps(devices)
    file_to_write.write(json_devices)
    file_to_write.close()


print(different_tags_order)





































