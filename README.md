# Usage

Enter into a virtual environment with `pipend shell`. Then install the dependencies with `pip install requests`, `pip instal requests_futures`, and `pip install matplotlib`. Then run `get_and_sort_data.py` to download data into `sorted_data.txt`, which is sorted by it's timestamp. The format of the data is one long json list. The files `device_count.py` and `plot.py` can now be run.
