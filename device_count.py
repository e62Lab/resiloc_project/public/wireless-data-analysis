with open("sorted_data.txt", "r") as data_file:
    import json
    data = json.loads(data_file.read())

from datetime import datetime

dates = []
macs = []
for point in data:
    dates.append(datetime.strptime(point["timestamp"], "%Y-%m-%d %H:%M:%S")) #2021-09-16 12:17:14   
    macs.append(point["payload"]["scanned_device"])

visible = dict()
length = dict()

for macs_at_date, date in zip(macs, dates):
    for vkey in visible:
        if not vkey in macs_at_date and visible[vkey] != False:
            length[vkey] = (date - visible[vkey]).total_seconds()
            visible[vkey] = False
    for mac in macs_at_date:
        visible[mac] = date


categories = dict()
for mac in length:
    if not length[mac] in categories:
        categories[length[mac]] = 0
    categories[length[mac]] += 1


from math import log

print("time spent\tnumber of\tlog of number")
print("connected (s)\tdevices\t\tof devices")
for category in sorted(categories):
    print(round(category), end="\t\t")
    num = categories[category]
    print(num, end=": \t\t")
    print(round(log(num)) * "&")
