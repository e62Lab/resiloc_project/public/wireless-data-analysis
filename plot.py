#get moving round width and recursion from arguments
import sys
if len(sys.argv) > 1:
    width = int(sys.argv[1])
else:
    width = 0
if len(sys.argv) > 2:
    recursion = int(sys.argv[2])
else:
    recursion = 1

#open the pre downloaded, pre sorted data
with open("sorted_data.txt", "r") as data_file:
    import json
    data = json.loads(data_file.read())

import matplotlib.pyplot as plt
from datetime import datetime

#mage the graph larger
plt.figure(figsize=(14,8)) 

def moving_round(curve, round_range=10, recursion=1):
    """Change each point to the average of a range of points left and right of the current one."""
    out = []
    for i in range(len(curve)):
        #make sure not to use points out of the range of the list
        round_left = round_range
        round_right = round_range
        if i < round_left:
            round_left = i
        if i + round_right > len(curve) - 1:
            round_right = len(curve) - 1 - i
        
        #do the rounding
        sector = curve[i-round_left:i+round_right+1]
        out.append(sum(sector)/len(sector))
    #call the function on the new rounded curve if recursion is on
    recursion -= 1
    if recursion > 0:
        return moving_round(out, round_range, recursion)
    return out

points = []    #number of ble devices visible
dates = []     #date in datetime.datetime format
int_dates = [] #date in int format (seconds since 1.1.2000
#this is used so matplotlib spaces the x axis correctly

for i in data:
    #add the number of connected devices to points
    points.append(i["payload"]["max_device_number"])
    #get the datetime.datetime object from the string in dates
    dates.append(datetime.strptime(i["timestamp"], "%Y-%m-%d %H:%M:%S")) #2021-09-16 12:17:14
    #get the number of seconds since 2000
    int_dates.append((dates[-1] - datetime(2000, 1, 1)).total_seconds()) #2021-09-16 12:17:14

#draw the raw data for comparison
#plt.plot(int_dates, moving_round(points, 0, recursion=1), "c")
#draw the rounded data
plt.plot(int_dates, moving_round(points, width, recursion=recursion), "b")

ticks = []  #locations of ticks, every tick can't be shown since that would be too many
labels = [] #list of strings, labels for the ticks above

for i, time in enumerate(dates):
    #only show full hour times
    if time.minute == 0: 
        timestr = str(time)[:16] #cut of seconds and milliseconds
        if not timestr in labels: #only draw one label even if there were multiple data points this minute
            ticks.append(int_dates[i]) #mark the tick as visible
            labels.append(timestr)     #add the string label

plt.xticks(ticks, labels=labels, rotation=90) #rotate the labels to be vertical
plt.tight_layout() #so that the labels arent cut off

#save the final file
if len(sys.argv) > 3:
    plt.savefig(sys.argv[3])
else:
    plt.savefig("test_graph_{}_{}.png".format(width, recursion))
plt.show()
