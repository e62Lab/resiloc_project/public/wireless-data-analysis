
'''
@brief Calculate "distance" from RSSI values based on exponential function base^(kx)
@param rssi_1 RSSI value 1
@param rssi_2 RSSI value 2
@return "distance" 
'''
def RSSI_dist_exp(rssi_1, rssi_2, base, k):
    abs_dist = abs(rssi_1-rssi_2)
    return pow(base, k*abs_dist) - 1


'''
@brief Calculate "distance" from RSSI values based on potential function x^(k)
@param rssi_1 RSSI value 1
@param rssi_2 RSSI value 2
@return "distance" 
'''
def RSSI_dist_pot(rssi_1, rssi_2, k):
    abs_dist = abs(rssi_1-rssi_2)
    return pow(abs_dist, k) 


'''
@brief Calculate "distance" from two UNIX timestamps
@param time_1 UNIX timestamp 1
@param time_2 UNIX timestamp 2
@param knee knee of function (for example 1)
@param scale Scale of number -1 of value of function below knee
@return "distance"
        |
       1|       _______________
        |       |
-------------------------
        |       |
      -1|________
        |
        |
'''
def timing_dist(time_1, time_2, knee, scale, offset):
    abs_dist = abs(time_1-time_2)
    if abs_dist > knee:
        return 1*scale + offset
    else:
        return -1*scale + offset


'''
@brief Calculate "distance" from two words that differ in some bits. Returned "distance" represents relative number of different bits to number of bits in words
@param word_1 Word 1
@param word_2 Word 2 
@param scale    Returned distance by default is [0,1]. With scale you can scale output
@return "distance"
'''
def bit_dist(word_1, word_2, scale, word_bit_length):
    xor = word_1 ^ word_2
    num_of_diff_bits = bin(xor).count('1')
    # num_of_same_bits = word_bit_length - num_of_diff_bits
    # return (num_of_diff_bits-num_of_same_bits)*scale
    return (num_of_diff_bits/float(word_bit_length)) * scale



'''
@brief List intersection
@param list_1 List 1
@param list_2 List 2 
@return Intersection of given lists in type list
'''
def lists_intersection(list_1, list_2):
    return list(set(list_1) & set(list_2))


'''
@brief Lists symmetric difference
@param list_1 List 1
@param list_2 List 2 
@return Symmetric difference of given lists in type list
'''
def lists_symmetric_difference(list_1, list_2):
    return list(set(list_1) ^ set(list_2))


def union_of_keys_in_dicts(dict_1, dict_2):
    temp_dict = dict_1.copy()
    temp_dict.update(dict_2)
    return temp_dict.keys()

def dicts_keys_intersection(dict_1, dict_2):
    # return dict(dict_1.items() & dict_2.items()) # checks if values are the same too!
    # return {key for key in dict_1 if key in dict_2}
    return lists_intersection(dict_1.keys(), dict_2.keys())

def dicts_keys_symmetric_difference(dict_1, dict_2):
    return lists_symmetric_difference(dict_1.keys(), dict_2.keys())

def union_of_lists_wo_repetitions(list_of_lists):
    sum_of_lists = []
    for sep_list in list_of_lists:
        sum_of_lists += sep_list
    return list(set(sum_of_lists))






